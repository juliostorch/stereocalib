#include "core.h"
#include "cvex.h"
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <QtCore>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

CalibData::CalibData( const cv::Mat &camera, const cv::Mat &dist, const cv::Mat &rotation,const  cv::Mat &translation, double error ){
    cameraMatrix = camera;
    distortion = dist;
    translationMatrix = translation;
    if (rotation.cols==1 || rotation.rows==1)
        Rodrigues(rotation, rotationMatrix);
    else
        rotationMatrix = rotation;
    reprojectionError = error;
}

CalibData& CalibData::operator=(const CalibData& b){
    cameraMatrix = b.cameraMatrix.clone();
    distortion = b.distortion.clone();
    rotationMatrix = b.rotationMatrix.clone();
    translationMatrix = b.translationMatrix.clone();
    reprojectionError = b.reprojectionError;

    return *this;
}

CalibData::CalibData( const CalibData &b ){
    cameraMatrix = b.cameraMatrix.clone();
    distortion = b.distortion.clone();
    rotationMatrix = b.rotationMatrix.clone();
    translationMatrix = b.translationMatrix.clone();
    reprojectionError = b.reprojectionError;
}

bool CalibData::isValid() const{
    return cameraMatrix.data!=NULL && distortion.data!=NULL && translationMatrix.data!=NULL && rotationMatrix.data!=NULL;
}

Mat CalibData::rotationVector() const{
    Mat v;
    Rodrigues(rotationMatrix, v);
    return v;
}

Mat CalibData::getProjectionMatrix() const {
    Mat P(3, 4, CV_64F);
    Mat aux = P.colRange(0, 3);
    rotationMatrix.copyTo( aux );
    aux = P.col(3);
    translationMatrix.copyTo(aux);
//    P = cameraMatrix*P;
    return P;
}

Mat CalibData::getIntrinsicCompact() const{
    Mat_<double> mat(1, 12);
    Mat_<double> cam = cameraMatrix;
    Mat_<double> dist = distortion.reshape(1, 1);
    mat = 0.;
    Mat k = (Mat_<double>(1,4) << cam(0,0), cam(1,1), cam(0,2), cam(1,2));
    Mat aux = mat.colRange(0, 4);
    k.copyTo(aux);
    aux = mat.colRange(4, 4+distortion.rows);
    dist.copyTo(aux);

    cout << "param=" << mat << endl;
    cout << "aux=" << aux << endl;
    cout << "dist=" << dist << endl;
    cout << "dist2=" << distortion << endl;

    return mat;
}

void CalibData::setIntrinsicCompact(Mat param){
    double *par = param.ptr<double>();
    cameraMatrix = (Mat_<double>(3,3) << par[0], 0, par[2], /**/ 0,  par[1], par[3], /**/ 0, 0, 1);
    Mat aux = param.colRange(4, 12);
    aux.copyTo(distortion);
}

CalibData CalibData::toCameraCoordinates() const{
    CalibData local = *this;
    local.rotationMatrix = Mat::eye(3,3, CV_64F);
    local.translationMatrix = Mat::zeros(3, 1, CV_64F);
    return local;
}

Mat CalibData::project(Mat points) const{
    Mat proj;
    if (isValid())
        projectPoints(points, rotationVector(), translationMatrix, cameraMatrix, distortion, proj);

    return proj;
}

void CalibData::project(const Mat &objPoints, Mat &imagePoints, Mat &jacobian) const{
    if (isValid())
        projectPoints(objPoints, rotationVector(), translationMatrix, cameraMatrix, distortion, imagePoints, jacobian);
}

void CalibData::setReference(const CalibData &r)
{
    rotationMatrix = rotationMatrix*r.rotationMatrix.t();
    translationMatrix = translationMatrix -rotationMatrix*r.translationMatrix;
}

//fx fy xc yc k1 k2 p1 p2 k3 r1 r2 r3 t1 t2 t3
cv::Mat CalibData::toVector() const{
    cv::Mat_<double> vec(1, 15);
    if (cameraMatrix.type() == CV_64F){
        vec(0) = cameraMatrix.at<double>(0,0);
        vec(1) = cameraMatrix.at<double>(1,1);
        vec(2) = cameraMatrix.at<double>(0,2);
        vec(3) = cameraMatrix.at<double>(1,2);
    } else {
        vec(0) = cameraMatrix.at<float>(0,0);
        vec(1) = cameraMatrix.at<float>(1,1);
        vec(2) = cameraMatrix.at<float>(0,2);
        vec(3) = cameraMatrix.at<float>(1,2);
    }

    double *v = (double*)vec.ptr();
    Mat d = distortion.t();
    Mat aux(1, 5, CV_64F, (uchar*)(v+4));
    d.colRange(0, 5).copyTo(aux);

    aux = Mat(3, 1, CV_64F, (uchar*)(v+9));
    Rodrigues(rotationMatrix, aux);

    aux = Mat(3, 1, CV_64F, (uchar*)(v+12));
    translationMatrix.copyTo(aux);


    return vec;
}


//t1 t2 t3 rx ry rz xc yc fx fy
cv::Mat CalibData::toSVEVector() const{
	cv::Mat_<double> vec(1, 10);

	//Parâmetros extrínsecos
	cv::Mat_d T = translationMatrix;
	cv::Mat R, Q, X, Y, Z;
	cv::RQDecomp3x3(rotationMatrix, R, Q, X, Y, Z);
	cv::Mat_d rvec;
	double x, y, z;
	cv::Rodrigues(X, rvec);
	x = cv::norm(rvec) ;
	cv::Rodrigues(Y, rvec);
	y = cv::norm(rvec);
	cv::Rodrigues(Z, rvec);
	z = cv::norm(rvec);
	vec(0) = T(0);
	vec(1) = T(1);
	vec(2) = T(2);
	vec(3) = x;
	vec(4) = y;
	vec(5) = z;


	//parâmetros intrínsecos
	if (cameraMatrix.type() == CV_64F){
		vec(6) = cameraMatrix.at<double>(0, 2);
		vec(7) = cameraMatrix.at<double>(1, 2);
		vec(8) = cameraMatrix.at<double>(0, 0);
		vec(9) = cameraMatrix.at<double>(1, 1);
	}
	else {
		vec(6) = cameraMatrix.at<float>(0, 2);
		vec(7) = cameraMatrix.at<float>(1, 2);
		vec(8) = cameraMatrix.at<float>(0, 0);
		vec(9) = cameraMatrix.at<float>(1, 1);
	}

	return vec;
}


CalibPattern::CalibPattern(){
    pointsPerRow=0;
    pointsPerColumn=0;
    origin=Point2f(0,0);
    xDistance=0;
    yDistance=0;
    adjust=false;
}

CalibPattern::CalibPattern( int pointsPerRow, int pointsPerColumn, cv::Point2f origin, float xDistance, float yDistance ){
    CalibPattern::pointsPerRow = pointsPerRow;
    CalibPattern::pointsPerColumn = pointsPerColumn;
    CalibPattern::origin = origin;
    CalibPattern::xDistance = xDistance;
    CalibPattern::yDistance = yDistance;
}

cv::Size CalibPattern::patternSize() const{
    return Size(pointsPerRow, pointsPerColumn);
}

int CalibPattern::numberOfPoints() const{
    return pointsPerRow*pointsPerColumn;
}

std::vector<cv::Point3f> CalibPattern::getPatternCenters() const{
    Point3f c(origin.x, origin.y, 0);
    vector<Point3f> obj;

    for(int j=0; j<pointsPerColumn; j++)
        for(int i=0; i<pointsPerRow; i++){
            Point3f p = c + Point3f(xDistance*i, yDistance*j, 0);
            if (adjust && i>=19 && i<=27)
                p.x -= 1.467;
            obj.push_back(p);
        }

    return obj;
}
std::vector<cv::Point3f> CalibPattern::getPatternCenters(const Mat &R, const Mat &T) const{
    Vec3f c(origin.x, origin.y, 0);
    vector<Point3f> obj;

    Matx33f _R = R;
    Matx31f _T = T;

    for(int j=0; j<pointsPerColumn; j++){
        for(int i=0; i<pointsPerRow; i++){
            Vec3f p = c + Vec3f(xDistance*i, yDistance*j, 0);
            if (adjust && i>=19 && i<=27)
                p[0] -= 1.467;
            Matx31f p2 = (_R*p + _T);
            obj.push_back(Point3f( p2(0,0), p2(1,0), p2(2,0) ));
        }
    }

    return obj;
}

std::vector<std::vector<cv::Point3f> > CalibPattern::getPatternCenters(int numberOfViews) const{
    std::vector<std::vector<cv::Point3f> > obj_centers;

    std::vector<Point3f> obj = getPatternCenters();
    for(int i=0; i<numberOfViews; i++)
        obj_centers.push_back(obj);

    return obj_centers;
}

bool CalibPattern::isValid() const{
	return pointsPerRow && pointsPerColumn && xDistance && yDistance;
}

Pair::Pair(const QString &left, const QString &right, const CalibPattern &p){
    leftFile = left;
    rightFile = right;
    pattern = p;
}

void Pair::findLeftGrid(){
    leftCenters = findGrid(leftFile, pattern.patternSize());
}

void Pair::findRightGrid(){
    rightCenters = findGrid(rightFile, pattern.patternSize());
}

bool Pair::isValid() const{
    return !leftFile.isEmpty() && !rightFile.isEmpty() && leftCenters.size() && rightCenters.size();
}

Pair& Pair::operator=(const Pair& b){
    leftFile = b.leftFile;
    leftCenters = b.leftCenters;
    rightFile = b.rightFile;
    rightCenters = b.rightCenters;
    translationMatrix = b.translationMatrix;
    rotationMatrix = b.rotationMatrix;
    pattern = b.pattern;

    return *this;
}




cv::FileStorage& operator<<( cv::FileStorage &fs, const CalibData &calib){
    fs << "{"
       << "cameraMatrix" << calib.cameraMatrix
       << "distortion" << calib.distortion
       << "rotationMatrix" << calib.rotationMatrix
       << "translationMatrix" << calib.translationMatrix
       << "reprojectionError" << calib.reprojectionError
       << "}";
    return fs;
}

void operator>>( const cv::FileNode &n, CalibData &calib ){
    n["cameraMatrix"] >> calib.cameraMatrix;
    n["distortion"] >> calib.distortion;
    n["rotationMatrix"] >> calib.rotationMatrix;
    n["translationMatrix"] >> calib.translationMatrix;
    n["reprojectionError"] >> calib.reprojectionError;
}


cv::FileStorage& operator<<( cv::FileStorage &fs, const CalibPattern &pattern){
    fs << "{"
       << "pointsPerRow" << pattern.pointsPerRow << "pointsPerColumn" << pattern.pointsPerColumn
       << "xOrigin" << pattern.origin.x << "yOrigin" << pattern.origin.y
       << "xDistance" << pattern.xDistance << "yDistance" << pattern.yDistance
       << "adjust" << pattern.adjust << "}";
    return fs;
}

void operator>>( const cv::FileNode &n, CalibPattern &pattern ){
    n["pointsPerRow"] >> pattern.pointsPerRow;
    n["pointsPerColumn"] >> pattern.pointsPerColumn;
    n["xOrigin"] >> pattern.origin.x;
    n["yOrigin"] >> pattern.origin.y;
    n["xDistance"] >> pattern.xDistance;
    n["yDistance"] >> pattern.yDistance;
    n["adjust"] >> pattern.adjust;
}

cv::FileStorage& operator<<( cv::FileStorage &fs, const Pair &pair){
    fs << "{"
       << "leftFile" << pair.leftFile.toStdString()
       << "rightFile" << pair.rightFile.toStdString()
       << "pattern" << pair.pattern
       << "rotationMatrix" << pair.rotationMatrix
       << "translationMatrix" << pair.translationMatrix
       << "leftCenters" << "[";
    for(size_t i=0; i<pair.leftCenters.size(); i++)
        fs << pair.leftCenters[i].x << pair.leftCenters[i].y;
    fs << "]";
    fs << "rightCenters" << "[";
    for(size_t i=0; i<pair.rightCenters.size(); i++)
        fs << pair.rightCenters[i].x << pair.rightCenters[i].y;
    fs << "]"
       << "}";

    return fs;
}

void operator>>( const cv::FileNode &n, Pair &pair ){
    pair.leftCenters.clear();
    pair.rightCenters.clear();

    pair.leftFile = QString::fromStdString( n["leftFile"] );
    pair.rightFile = QString::fromStdString( n["rightFile"] );
    n["pattern"] >> pair.pattern;
    n["rotationMatrix"] >> pair.rotationMatrix;
    n["translationMatrix"] >> pair.translationMatrix;

    cv::FileNode node = n["leftCenters"];
    for(size_t i=0; i<node.size(); i+=2)
        pair.leftCenters.push_back( Point2f( node[i], node[i+1] ) );

    node = n["rightCenters"];
    for(size_t i=0; i<node.size(); i+=2)
        pair.rightCenters.push_back( Point2f( node[i], node[i+1] ) );

}




std::vector<cv::Point2f> findGrid(Mat img, const cv::Size& patternSize){
	std::vector<Point2f> centers;
	centers.resize(patternSize.width*patternSize.height);

	if (!findCirclesGrid(img, patternSize, centers, CALIB_CB_SYMMETRIC_GRID))
		centers.clear();
	return centers;
}

std::vector<cv::Point2f> findGrid(const QString &fileName, const cv::Size& patternSize){
    std::vector<Point2f> centers;
    centers.resize(patternSize.width*patternSize.height);
    Mat img = imread(fileName.toStdString());

    if (!findCirclesGrid(img, patternSize, centers, CALIB_CB_SYMMETRIC_GRID))
        centers.clear();
    return centers;
}

CalibData calibrate( cv::Size imageSize, const std::vector<std::vector<cv::Point2f> > &imageCenters,std::vector<std::vector<cv::Point3f> > &patternCenters, int view ){
    Mat K, dist;
    vector<Mat> rvecs, tvecs;
    K = Mat::eye(3, 3, CV_64F);
    dist = Mat::zeros(8, 1, CV_64F);

    double error = calibrateCamera(patternCenters, imageCenters, imageSize, K, dist, rvecs, tvecs, CV_CALIB_ZERO_TANGENT_DIST | CV_CALIB_FIX_K3 |
                                   CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 | CV_CALIB_FIX_K6 | CV_CALIB_FIX_K1 | CV_CALIB_FIX_K2);
    return CalibData( K, dist, rvecs[view], tvecs[view], error);
}

void calibrate( cv::Size imageSize, CalibData &leftCalib, CalibData &rightCalib, const std::vector<std::vector<cv::Point2f> > &leftCenters,
                const std::vector<std::vector<cv::Point2f> > &rightCenters, const std::vector<std::vector<cv::Point3f> > &patternCenters )
{
    Mat R, T, E, F;

    double error = stereoCalibrate(patternCenters, leftCenters, rightCenters, leftCalib.cameraMatrix, leftCalib.distortion, rightCalib.cameraMatrix, rightCalib.distortion, imageSize, R, T, E, F,
                                   TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 100, 1e-12));
    leftCalib.reprojectionError = error;
    rightCalib.reprojectionError = error;

    leftCalib.rotationMatrix = Mat::eye(3, 3, CV_64F);
    leftCalib.translationMatrix = Mat::zeros(3, 1, CV_64F);
    rightCalib.rotationMatrix = R;
    rightCalib.translationMatrix = T;
}

void calibrate( const std::vector<Pair> &pairs, CalibData &leftCalib, CalibData &rightCalib ){
    std::vector<std::vector<cv::Point2f> > leftCenters;
    std::vector<std::vector<cv::Point2f> > rightCenters;
    std::vector<std::vector<cv::Point3f> > patternCenters;

    for(size_t i=0; i<pairs.size(); i++){
        if (pairs[i].isValid()){
            leftCenters.push_back( pairs[i].leftCenters );
            rightCenters.push_back( pairs[i].rightCenters );
            patternCenters.push_back( pairs[i].pattern.getPatternCenters() );
        }
    }

    Size s= imread(pairs[0].leftFile.toStdString()).size();
    leftCalib = calibrate(s, leftCenters, patternCenters);
    rightCalib = calibrate(s, rightCenters, patternCenters);
    calibrate(s, leftCalib, rightCalib, leftCenters, rightCenters, patternCenters);
}

std::vector<cv::Point3f> triangulate(const CalibData &leftCalib, const CalibData &rightCalib, const std::vector<cv::Point2f> &leftPoints, const std::vector<cv::Point2f> &rightPoints){
    Mat points;
    Mat leftNormalized, rightNormalized;

    undistortPoints(leftPoints, leftNormalized, leftCalib.cameraMatrix, leftCalib.distortion, noArray());//, leftCalib.toCameraCoordinates().getProjectionMatrix());
    undistortPoints(rightPoints, rightNormalized, rightCalib.cameraMatrix, rightCalib.distortion, noArray());//, rightCalib.toCameraCoordinates().getProjectionMatrix());

    triangulatePoints(leftCalib.getProjectionMatrix(), rightCalib.getProjectionMatrix(),
                      leftNormalized.reshape(1, leftPoints.size()).t(), rightNormalized.reshape(1, rightPoints.size()).t(), points);
    points.row(0) = points.row(0) / points.row(3);
    points.row(1) = points.row(1) / points.row(3);
    points.row(2) = points.row(2) / points.row(3);

    Mat_f p = points.rowRange(0, 3).t();

    std::vector<cv::Point3f> pt;
    for(int i=0; i<p.rows; i++)
        pt.push_back(p.at<Point3f>(i, 0));

    return pt;
}

void verify2( const std::vector<Pair> pairs, const CalibData &leftCalib, const CalibData &rightCalib, float &media, float &maximo, float maximo_percent ){
    std::vector<std::vector<cv::Point3f> > points = triangulate(leftCalib, rightCalib, pairs, false);
    QVector<float> erros;


    for(size_t i=0; i<pairs.size(); i++){
        vector<float> medido, real, diff;
        medido = calcDistances(points[i]);
        real = calcDistances(pairs[i].pattern.getPatternCenters());
        diff.resize(medido.size());
        Mat _diff = (Mat)diff;
        divide(abs((Mat)real - (Mat)medido), (Mat)real, _diff);
        erros << QVector<float>::fromStdVector(diff);
    }

    vector<float> sorted = erros.toStdVector();
    cv::sort(sorted, cv::LessThan<float>());

    media = mean(sorted)[0];
    int id = cvRound(sorted.size()*maximo_percent);
    maximo = sorted[id-1];

}

void verify2(const std::vector<Pair> pairs, const CalibData &leftCalib, const CalibData &rightCalib, const char *filename){
    std::vector<std::vector<cv::Point3f> > points = triangulate(leftCalib, rightCalib, pairs, false);

    std::fstream arq;
    arq.open(filename, std::ios_base::out);
    for(size_t i=0; i<pairs.size(); i++){
        cv::Mat R, T;

        estimateRigidMotion( pairs[i].pattern.getPatternCenters(), points[i], R, T );

        arq << "vista(" << i+1 << ").pt=" << cv::Mat(points[i]) << ";" << std::endl;
        arq << "vista(" << i+1 << ").grid=" << cv::Mat(pairs[i].pattern.getPatternCenters()) << ";" << std::endl;
        arq << "vista(" << i+1 << ").file=" << "'" << pairs[i].leftFile.toStdString() << "';" << std::endl;
        arq << "vista(" << i+1 << ").R=" << R << ";" << std::endl;
        arq << "vista(" << i+1 << ").T=" << T << ";" << std::endl;
        arq << "vista(" << i+1 << ").grid2=" << cv::Mat(pairs[i].pattern.getPatternCenters(R, T)) << ";" << std::endl;
    }

    float media, maximo;
    verify2(pairs, leftCalib, rightCalib, media, maximo);

    arq << "R=" << rightCalib.rotationMatrix << ";" << std::endl;
    arq << "T=" << rightCalib.translationMatrix << ";" << std::endl;
    arq << "Kr=" << rightCalib.cameraMatrix << ";" << std::endl;
    arq << "Dr=" << rightCalib.distortion << ";" << std::endl;
    arq << "Kl=" << leftCalib.cameraMatrix << ";" << std::endl;
    arq << "Dl=" << leftCalib.distortion << ";" << std::endl;
    arq << "media="  << media << ";" << std::endl;
    arq << "maximo="  << maximo << ";" << std::endl;
}

void refineMatch( Point2f &leftPoint, Point2f &rightPoint, const Mat &leftImage, const Mat &rightImage ){
    vector<KeyPoint> leftKeypoints, rightKeypoints;
    for( int i=-10; i<11; i++ ){
        for( int j=-10; j<11; j++){
            rightKeypoints.push_back( KeyPoint(rightPoint+Point2f(i, j), 21) );
        }
    }

    leftKeypoints.push_back( KeyPoint(leftPoint, 21) );
    SurfDescriptorExtractor extractor;
    Mat leftDescritpors, rightDescriptors;
    extractor.compute(leftImage, leftKeypoints, leftDescritpors);
    extractor.compute(rightImage, rightKeypoints, rightDescriptors);

    BFMatcher matcher(NORM_L2, false);
    vector<DMatch> matches;
    matcher.match(leftDescritpors, rightDescriptors, matches);

    std::sort(matches.begin(), matches.end());

    leftPoint = leftKeypoints[matches[0].queryIdx].pt;
    rightPoint = rightKeypoints[matches[0].trainIdx].pt;
}


void refineMatches( std::vector<cv::Point2f> &leftPoints, std::vector<cv::Point2f> &rightPoints, const QString &leftFile, const QString &rightFile){
    Mat leftImage = imread(leftFile.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
    Mat rightImage = imread(rightFile.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);

    for(size_t i=0; i<leftPoints.size(); i++){
        refineMatch(leftPoints[i], rightPoints[i], leftImage, rightImage);
    }
}

std::vector<float> calcDistances(std::vector<cv::Point3f> points){
    std::vector<float> dist;

    for(size_t i=0; i<points.size(); i++)
        for( size_t j=i+1; j<points.size(); j++ )
            dist.push_back( norm(points[i]-points[j]) );

    return dist;
}

void findMatches( const QString &leftFile, const QString &rightFile, const CalibData &leftCalib, const CalibData &rightCalib, std::vector<Point2f> &leftPoints, std::vector<Point2f> &rightPoints,
                  int nfeatures, int nOctaveLayers, double contrastThreshold, double edgeThreshold, double sigma){

    Mat leftImage = imread(leftFile.toStdString(), 0);
    Mat rightImage = imread(rightFile.toStdString(), 0);

    vector<KeyPoint> leftKp, rightKp;
    Mat leftDescriptors, rightDescriptors;
    SIFT sift(nfeatures, nOctaveLayers, contrastThreshold, edgeThreshold, sigma);
    sift(leftImage, cv::noArray(), leftKp, leftDescriptors);
    sift(rightImage, cv::noArray(), rightKp, rightDescriptors);

    FlannBasedMatcher matcher;
    vector<vector<DMatch> > selfMatches;

    vector<DMatch> matches;
    matcher.knnMatch(leftDescriptors, rightDescriptors, selfMatches, 2);
    matcher.match(leftDescriptors, rightDescriptors, matches);

//    leftPoints.clear();
//    rightPoints.clear();

    float distance=0;
    for(size_t i=0; i<selfMatches.size(); i++)
        distance += selfMatches[i][1].distance;
    float th = 0.7*distance/selfMatches.size();

//    for(size_t i=0; i<matches1.size(); i++){
//        DMatch m = matches1[i];
//        DMatch m2 = matches2[ m.trainIdx ];
//        if (m.distance>th && m2.trainIdx==i ){
//            Point2f p = leftKp[ m.queryIdx ].pt;
//            leftPoints.push_back( QPointF(p.x, p.y) );
//            p = rightKp[ m.trainIdx ].pt;
//            rightPoints.push_back( QPointF(p.x, p.y) );
//        }
//    }

    vector<Point2f> leftpt, rightpt;
    for(size_t i=0; i<matches.size(); i++){
        DMatch m = matches[i];
        if (m.distance<th){
            leftpt.push_back( leftKp[ m.queryIdx ].pt );
            rightpt.push_back( rightKp[ m.trainIdx ].pt );
        }
    }

    Mat mask(1, leftpt.size(),  CV_8U);
    findFundamentalMat(leftpt, rightpt, FM_RANSAC, 3, 0.99, mask);
    for(size_t i=0; i<leftpt.size(); i++){
        if (mask.at<uchar>(0,i)==1){
            leftPoints.push_back( leftpt[i] );
            rightPoints.push_back( rightpt[i] );
        }
    }


}
