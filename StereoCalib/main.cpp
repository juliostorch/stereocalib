﻿#include "stereocalib.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QCoreApplication::setOrganizationName("Petrobras");
	QCoreApplication::setApplicationName("StereoCalib");
	StereoCalib w;
	w.show();
	return a.exec();
}
