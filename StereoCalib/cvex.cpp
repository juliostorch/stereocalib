#include "cvex.h"
#include <qimage.h>

using namespace cv;
using namespace std;

//void cv::convertPointsHomogeneous( const cv::Mat &src, cv::Mat &dst){
//    Mat in = src.reshape(1);
//    assert( (src.cols == 3 || src.cols == 2) && (src.type() == CV_32FC1 || src.type() == CV_64FC1) );

//    Mat out = dst.reshape(1);
//    bool dst_ok = src.type() == dst.type();

//    if (src.cols == 2){
//        dst_ok = dst_ok && dst.cols == 3;
//        if (!dst_ok)
//            out = Mat( in.rows, 3, in.type() );
//        Mat aux = out.colRange(0, 2);
//        in.copyTo( aux );
//        out.col(2) = 1;
//        if (src.channels()==2)
//            out.reshape(3);
//    } else {
//        dst_ok = dst_ok && dst.cols == 2;
//        if (!dst_ok)
//            out = Mat( in.rows, 2, in.type() );
//        out.col(0) = in.col(0) / in.col(2);
//        out.col(1) = in.col(1) / in.col(2);
//        if (src.channels()==3)
//            out.reshape(2);
//    }

//    dst = out;
//}

void cv::convertPointsHomogeneous3D( const cv::Mat &src, cv::Mat &dst){
    Mat in = src.reshape(1);
    assert( (src.cols == 4 || src.cols == 3) && (src.type() == CV_32FC1 || src.type() == CV_64FC1) );

    Mat out = dst.reshape(1);
    bool dst_ok = src.type() == dst.type();

    if (src.cols == 3){
        dst_ok = dst_ok && dst.cols == 4;
        if (!dst_ok)
            out = Mat( in.rows, 4, in.type() );
        Mat aux = out.colRange(0, 3);
        in.copyTo( aux );
        out.col(3) = 1;
        if (src.channels()==3)
            out.reshape(4);
    } else {
        dst_ok = dst_ok && dst.cols == 3;
        if (!dst_ok)
            out = Mat( in.rows, 3, in.type() );
        out.col(0) = in.col(0) / in.col(3);
        out.col(1) = in.col(1) / in.col(3);
        out.col(2) = in.col(2) / in.col(3);
        if (src.channels()==4)
            out.reshape(3);
    }

    dst = out;
}
/*
ostream& cv::operator<<( ostream& out, const Mat& mat ) {
    //CV_Assert( mat.type() == CV_32FC1 || mat.type() == CV_64FC1 )

    //Encontra ordem de grandeza do maior elemento.
    double minVal, maxVal;
    try{
        minMaxLoc(mat, &minVal, &maxVal);
    } catch(...) {
        minVal = maxVal = 1;
    }

    minVal = fabs( minVal );
    double m = max<double>( minVal, maxVal );
    double exp = floor(log10(m));
    double div = pow( 10, exp );

    ios_base::fmtflags flags = out.flags();
    out << fixed;
    out.precision(3);

    if (exp != 0)
        out << "1e" << (int)exp << " * [" << endl;
    else
        out << '[' << endl;

    if (mat.type() == CV_64FC1)
    {
        MatConstIterator_<double> it = mat.begin<double>();

        for (int i=0; i<mat.rows; i++){
            for (int j=0; j<mat.cols; j++, it++) {
                out.width(10);
                out << (*it)/div;
            }
            out << ';' << endl;
        }
        out << "];" << endl;
    }
    else if (mat.type() == CV_32FC1)
    {
        MatConstIterator_<float> it = mat.begin<float>();

        for (int i=0; i<mat.rows; i++){
            for (int j=0; j<mat.cols; j++, it++) {
                out.width(10);
                out << (*it)/div;
            }
            out << ';' << endl;
        }
        out << "];" << endl;
    }
    else if (mat.type() == CV_8SC1)
    {
        MatConstIterator_<char> it = mat.begin<char>();

        for (int i=0; i<mat.rows; i++){
            for (int j=0; j<mat.cols; j++, it++) {
                out.width(10);
                out << (int)*it;
            }
            out << ';' << endl;
        }
        out << "];" << endl;
    }
    else if (mat.type() == CV_8UC1)
    {
        MatConstIterator_<uchar> it = mat.begin<uchar>();

        for (int i=0; i<mat.rows; i++){
            for (int j=0; j<mat.cols; j++, it++) {
                out.width(10);
                out << (int)*it;
            }
            out << ';' << endl;
        }
        out << "];" << endl;
    }


    out.flags( flags );
    return out;
}
*/

void operator<<(QImage& img, const cv::Mat& mat){
	if (mat.type() == CV_8U)
		img = QImage(mat.data, mat.cols, mat.rows, QImage::Format_RGB888);
}

void saveImage(QImage &img, const cv::Mat& mat){
	if (mat.type() == CV_8U)
		img = QImage(mat.data, mat.cols, mat.rows, QImage::Format_RGB888);
}

