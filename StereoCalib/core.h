#ifndef CORE_H
#define CORE_H

#include <opencv2/core/core.hpp>
#include <QThread>
#include <QtCore>
#include <levmar.h>

class CalibData
{

public:
    cv::Mat cameraMatrix;
    cv::Mat distortion;
    cv::Mat rotationMatrix;
    cv::Mat translationMatrix;
    double reprojectionError;

    CalibData( const CalibData &c );
    CalibData(){reprojectionError=0;}
    CalibData( const cv::Mat &camera, const cv::Mat &dist, const cv::Mat &rotation,const  cv::Mat &translation, double error );
    bool isValid() const;
    cv::Mat rotationVector() const;
    cv::Mat getProjectionMatrix() const;
    cv::Mat getIntrinsicCompact() const;
    void setIntrinsicCompact(cv::Mat par);
    CalibData toCameraCoordinates() const;
    cv::Mat project(cv::Mat points) const;
    void project(const cv::Mat &objPoints, cv::Mat &imagePoints, cv::Mat &jacobian) const;
    void setReference( const CalibData &r);
    cv::Mat toVector() const;
	cv::Mat toSVEVector() const;

    CalibData& operator=(const CalibData& b);
};

class CalibPattern
{
public:
    int pointsPerRow;
    int pointsPerColumn;
    cv::Point2f origin;
    float xDistance;
    float yDistance;
    bool adjust;

    CalibPattern();
    CalibPattern( int pointsPerRow, int pointsPerColumn, cv::Point2f origin, float xDistance, float yDistance );

    cv::Size patternSize() const;
    int numberOfPoints() const;
    std::vector<cv::Point3f> getPatternCenters() const;
    std::vector<cv::Point3f> getPatternCenters(const cv::Mat &R, const cv::Mat &T) const;
    std::vector<std::vector<cv::Point3f> > getPatternCenters(int numberOfViews) const ;
	bool isValid() const;
};

class Pair{
public:
    QString leftFile;
    QString rightFile;
    CalibPattern pattern;
    std::vector<cv::Point2f> leftCenters;
    std::vector<cv::Point2f> rightCenters;
    cv::Mat rotationMatrix;
    cv::Mat translationMatrix;

    Pair(){}
    Pair( const QString &left, const QString &right, const CalibPattern &p );

    void findLeftGrid();
    void findRightGrid();
    bool isValid() const;

    Pair& operator=(const Pair& b);
};

cv::FileStorage& operator<<( cv::FileStorage &fs, const CalibData &calib);
void operator>>( const cv::FileNode &n, CalibData &calib );

cv::FileStorage& operator<<( cv::FileStorage &fs, const CalibPattern &calib);
void operator>>( const cv::FileNode &n, CalibPattern &calib );

cv::FileStorage& operator<<( cv::FileStorage &fs, const Pair &pair);
void operator>>( const cv::FileNode &n, Pair &pair );

template<typename T>
std::vector<T>& operator<<( std::vector<T> &self, const std::vector<T> &other ){
    int N = self.size();
    self.resize( N + other.size() );
    for(size_t i=0; i<other.size(); i++)
        self[N+i] = other[i];

    return self;
}

std::vector<cv::Point2f> findGrid(cv::Mat img, const cv::Size& patternSize);

std::vector<cv::Point2f> findGrid(const QString &fileName, const cv::Size& patternSize);

CalibData calibrate( cv::Size imageSize, const std::vector<std::vector<cv::Point2f> > &imageCenters,std::vector<std::vector<cv::Point3f> > &patternCenters, int view=0 );

void calibrate( cv::Size imageSize, CalibData &leftCalib, CalibData &rightCalib, const std::vector<std::vector<cv::Point2f> > &leftCenters,
                const std::vector<std::vector<cv::Point2f> > &rightCenters, const std::vector<std::vector<cv::Point3f> > &patternCenters );

void calibrate( const std::vector<Pair> &pairs, CalibData &leftCalib, CalibData &rightCalib );

std::vector<cv::Point3f> triangulate(const CalibData &leftCalib, const CalibData &rightCalib, const std::vector<cv::Point2f> &leftPoints, const std::vector<cv::Point2f> &rightPoints);
std::vector<std::vector<cv::Point3f> > triangulate(const CalibData &leftCalib, const CalibData &rightCalib, const std::vector<Pair> &pairs, bool adjust=false);

void estimateRigidMotion( const std::vector<cv::Point3f> &x, const std::vector<cv::Point3f> &y, cv::Mat &R, cv::Mat &T);

void recoverPatternPose( std::vector<Pair> &pairs, CalibData &leftCalib, CalibData &rightCalib );

std::vector<Pair> generatePairs( const std::vector<Pair> &ref, const CalibData &leftCalib, const CalibData &rightCalib, float gridError, float cameraError );


void verify2( const std::vector<Pair> pairs, const CalibData &leftCalib, const CalibData &rightCalib, float &media, float &maximo, float maximo_percent=0.99 );
void verify2( const std::vector<Pair> pairs, const CalibData &leftCalib, const CalibData &rightCalib, const char *filename );

void refineMatches( std::vector<cv::Point2f> &leftPoints, std::vector<cv::Point2f> &rightPoints, const QString &leftFile, const QString &rightFile);

std::vector<float> calcDistances(std::vector<cv::Point3f> points);


void findMatches( const QString &leftFile, const QString &rightFile, const CalibData &leftCalib, const CalibData &rightCalib, std::vector<cv::Point2f> &leftPoints, std::vector<cv::Point2f> &rightPoints,
                  int nfeatures=0, int nOctaveLayers=3, double contrastThreshold=0.04, double edgeThreshold=10, double sigma=1.6);

void refineCam(CalibData &calib, std::vector<cv::Point3f> objPoints, std::vector<cv::Point2f> imagePoints);

void refinePair( CalibData &leftCalib, CalibData &rightCalib, const std::vector<cv::Point2f> &leftPoints, const std::vector<cv::Point2f> &rightPoints );
void refinePair( CalibData &leftCalib, CalibData &rightCalib, const std::vector<cv::Point2f> &leftPoints, const std::vector<cv::Point2f> &rightPoints, const std::vector<cv::Point3f> &points );

void project( double *aj, double *bi, double *xij, const CalibData &calib);

#endif // CORE_H
