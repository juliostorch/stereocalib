#include "view.h"
#include <QtGui>
#include <QtCore>

View::View(QWidget *parent)
    :QGraphicsView(parent){
    mousePressed = false;
}

View::~View(){

}


void View::wheelEvent(QWheelEvent *event){
	if (event->modifiers().testFlag(Qt::ControlModifier)){
		ViewportAnchor anchor = transformationAnchor();
		setTransformationAnchor(AnchorUnderMouse);
		int d = event->delta() / 15;

		if (d == 0)
			scale(1, 1);

		if (d < 0)
			scale(0.8, 0.8);

		if (d>0)
			scale(1.25, 1.25);
		event->accept();
		setTransformationAnchor(anchor);
	}
	else
		QGraphicsView::wheelEvent(event);
}

void View::mousePressEvent(QMouseEvent *event){
    if (dragMode() == NoDrag){
        mousePressed = true;
        emit clicked( itemAt(event->pos()), mapToScene(event->pos()) );
    }
    else
        QGraphicsView::mousePressEvent(event);
}

void View::mouseMoveEvent(QMouseEvent *event){
    if (mousePressed)
        emit dragged( mapToScene( event->pos() ));
    else
        QGraphicsView::mouseMoveEvent(event);
}

void View::mouseReleaseEvent(QMouseEvent *event){
	if (mousePressed){
		mousePressed = false;
		emit released(itemAt(event->pos()), mapToScene(event->pos()));
	}
	QGraphicsView::mouseReleaseEvent(event);
}
