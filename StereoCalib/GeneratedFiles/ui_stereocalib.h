/********************************************************************************
** Form generated from reading UI file 'stereocalib.ui'
**
** Created by: Qt User Interface Compiler version 5.3.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STEREOCALIB_H
#define UI_STEREOCALIB_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "view.h"

QT_BEGIN_NAMESPACE

class Ui_StereoCalibClass
{
public:
    QAction *actionAbrir;
    QAction *actionSalvar;
    QAction *actionNovo;
    QAction *actionSalvar_Como;
    QAction *actionVisualizar_Imagens;
    QAction *actionCalibrar;
    QAction *actionSobre;
    QAction *actionBuscar_Padroes;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *pontosPorColunaLabel;
    QLineEdit *ptLinha;
    QLabel *label;
    QLineEdit *ptColuna;
    QLabel *label_2;
    QLineEdit *xOrigem;
    QLabel *label_3;
    QLineEdit *yOrigem;
    QLabel *l_padrao;
    QLabel *distNciaLabel;
    QLineEdit *dHorizontal;
    QLabel *distNciaVerticalLabel;
    QLineEdit *dVertical;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *bt_adiciona;
    QToolButton *bt_remove;
    QToolButton *bt_habilita;
    QToolButton *bt_desabilita;
    QToolButton *bt_limpa;
    QListView *img_view;
    QMenuBar *menuBar;
    QMenu *menuArquivo;
    QMenu *menuExibir;
    QMenu *menuEditar;
    QMenu *menuAjuda;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *dw_visualizacao;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_2;
    View *view;

    void setupUi(QMainWindow *StereoCalibClass)
    {
        if (StereoCalibClass->objectName().isEmpty())
            StereoCalibClass->setObjectName(QStringLiteral("StereoCalibClass"));
        StereoCalibClass->resize(1206, 1170);
        actionAbrir = new QAction(StereoCalibClass);
        actionAbrir->setObjectName(QStringLiteral("actionAbrir"));
        actionSalvar = new QAction(StereoCalibClass);
        actionSalvar->setObjectName(QStringLiteral("actionSalvar"));
        actionNovo = new QAction(StereoCalibClass);
        actionNovo->setObjectName(QStringLiteral("actionNovo"));
        actionSalvar_Como = new QAction(StereoCalibClass);
        actionSalvar_Como->setObjectName(QStringLiteral("actionSalvar_Como"));
        actionVisualizar_Imagens = new QAction(StereoCalibClass);
        actionVisualizar_Imagens->setObjectName(QStringLiteral("actionVisualizar_Imagens"));
        actionVisualizar_Imagens->setCheckable(true);
        actionCalibrar = new QAction(StereoCalibClass);
        actionCalibrar->setObjectName(QStringLiteral("actionCalibrar"));
        actionSobre = new QAction(StereoCalibClass);
        actionSobre->setObjectName(QStringLiteral("actionSobre"));
        actionBuscar_Padroes = new QAction(StereoCalibClass);
        actionBuscar_Padroes->setObjectName(QStringLiteral("actionBuscar_Padroes"));
        centralWidget = new QWidget(StereoCalibClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout = new QFormLayout(groupBox);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        pontosPorColunaLabel = new QLabel(groupBox);
        pontosPorColunaLabel->setObjectName(QStringLiteral("pontosPorColunaLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, pontosPorColunaLabel);

        ptLinha = new QLineEdit(groupBox);
        ptLinha->setObjectName(QStringLiteral("ptLinha"));

        formLayout->setWidget(0, QFormLayout::FieldRole, ptLinha);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        ptColuna = new QLineEdit(groupBox);
        ptColuna->setObjectName(QStringLiteral("ptColuna"));

        formLayout->setWidget(1, QFormLayout::FieldRole, ptColuna);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        xOrigem = new QLineEdit(groupBox);
        xOrigem->setObjectName(QStringLiteral("xOrigem"));

        formLayout->setWidget(4, QFormLayout::FieldRole, xOrigem);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_3);

        yOrigem = new QLineEdit(groupBox);
        yOrigem->setObjectName(QStringLiteral("yOrigem"));

        formLayout->setWidget(5, QFormLayout::FieldRole, yOrigem);

        l_padrao = new QLabel(groupBox);
        l_padrao->setObjectName(QStringLiteral("l_padrao"));
        l_padrao->setScaledContents(false);
        l_padrao->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(8, QFormLayout::SpanningRole, l_padrao);

        distNciaLabel = new QLabel(groupBox);
        distNciaLabel->setObjectName(QStringLiteral("distNciaLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, distNciaLabel);

        dHorizontal = new QLineEdit(groupBox);
        dHorizontal->setObjectName(QStringLiteral("dHorizontal"));

        formLayout->setWidget(2, QFormLayout::FieldRole, dHorizontal);

        distNciaVerticalLabel = new QLabel(groupBox);
        distNciaVerticalLabel->setObjectName(QStringLiteral("distNciaVerticalLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, distNciaVerticalLabel);

        dVertical = new QLineEdit(groupBox);
        dVertical->setObjectName(QStringLiteral("dVertical"));

        formLayout->setWidget(3, QFormLayout::FieldRole, dVertical);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        bt_adiciona = new QToolButton(groupBox_2);
        bt_adiciona->setObjectName(QStringLiteral("bt_adiciona"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/edit_add.png"), QSize(), QIcon::Normal, QIcon::Off);
        bt_adiciona->setIcon(icon);
        bt_adiciona->setIconSize(QSize(32, 32));
        bt_adiciona->setAutoRaise(true);

        horizontalLayout->addWidget(bt_adiciona);

        bt_remove = new QToolButton(groupBox_2);
        bt_remove->setObjectName(QStringLiteral("bt_remove"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/Resources/edit_remove.png"), QSize(), QIcon::Normal, QIcon::Off);
        bt_remove->setIcon(icon1);
        bt_remove->setIconSize(QSize(32, 32));
        bt_remove->setAutoRaise(true);

        horizontalLayout->addWidget(bt_remove);

        bt_habilita = new QToolButton(groupBox_2);
        bt_habilita->setObjectName(QStringLiteral("bt_habilita"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/Resources/edit_check.png"), QSize(), QIcon::Normal, QIcon::Off);
        bt_habilita->setIcon(icon2);
        bt_habilita->setIconSize(QSize(32, 32));
        bt_habilita->setAutoRaise(true);

        horizontalLayout->addWidget(bt_habilita);

        bt_desabilita = new QToolButton(groupBox_2);
        bt_desabilita->setObjectName(QStringLiteral("bt_desabilita"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/Resources/edit_uncheck.png"), QSize(), QIcon::Normal, QIcon::Off);
        bt_desabilita->setIcon(icon3);
        bt_desabilita->setIconSize(QSize(32, 32));
        bt_desabilita->setAutoRaise(true);

        horizontalLayout->addWidget(bt_desabilita);

        bt_limpa = new QToolButton(groupBox_2);
        bt_limpa->setObjectName(QStringLiteral("bt_limpa"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/Resources/edit_clear.png"), QSize(), QIcon::Normal, QIcon::Off);
        bt_limpa->setIcon(icon4);
        bt_limpa->setIconSize(QSize(32, 32));
        bt_limpa->setAutoRaise(true);

        horizontalLayout->addWidget(bt_limpa);


        verticalLayout->addLayout(horizontalLayout);

        img_view = new QListView(groupBox_2);
        img_view->setObjectName(QStringLiteral("img_view"));
        img_view->setSelectionMode(QAbstractItemView::ExtendedSelection);
        img_view->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout->addWidget(img_view);


        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        StereoCalibClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(StereoCalibClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1206, 47));
        menuArquivo = new QMenu(menuBar);
        menuArquivo->setObjectName(QStringLiteral("menuArquivo"));
        menuExibir = new QMenu(menuBar);
        menuExibir->setObjectName(QStringLiteral("menuExibir"));
        menuEditar = new QMenu(menuBar);
        menuEditar->setObjectName(QStringLiteral("menuEditar"));
        menuAjuda = new QMenu(menuBar);
        menuAjuda->setObjectName(QStringLiteral("menuAjuda"));
        StereoCalibClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(StereoCalibClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        StereoCalibClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(StereoCalibClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        StereoCalibClass->setStatusBar(statusBar);
        dw_visualizacao = new QDockWidget(StereoCalibClass);
        dw_visualizacao->setObjectName(QStringLiteral("dw_visualizacao"));
        dw_visualizacao->setMinimumSize(QSize(640, 960));
        dw_visualizacao->setFeatures(QDockWidget::DockWidgetFloatable|QDockWidget::DockWidgetMovable);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout_2 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        view = new View(dockWidgetContents);
        view->setObjectName(QStringLiteral("view"));
        view->setDragMode(QGraphicsView::ScrollHandDrag);

        verticalLayout_2->addWidget(view);

        dw_visualizacao->setWidget(dockWidgetContents);
        StereoCalibClass->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dw_visualizacao);
#ifndef QT_NO_SHORTCUT
        pontosPorColunaLabel->setBuddy(ptLinha);
        label->setBuddy(ptColuna);
        label_2->setBuddy(xOrigem);
        label_3->setBuddy(yOrigem);
        distNciaLabel->setBuddy(dHorizontal);
        distNciaVerticalLabel->setBuddy(dVertical);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(ptLinha, ptColuna);
        QWidget::setTabOrder(ptColuna, dHorizontal);
        QWidget::setTabOrder(dHorizontal, dVertical);
        QWidget::setTabOrder(dVertical, xOrigem);
        QWidget::setTabOrder(xOrigem, yOrigem);
        QWidget::setTabOrder(yOrigem, img_view);
        QWidget::setTabOrder(img_view, bt_adiciona);
        QWidget::setTabOrder(bt_adiciona, bt_remove);
        QWidget::setTabOrder(bt_remove, bt_habilita);
        QWidget::setTabOrder(bt_habilita, bt_desabilita);
        QWidget::setTabOrder(bt_desabilita, bt_limpa);

        menuBar->addAction(menuArquivo->menuAction());
        menuBar->addAction(menuEditar->menuAction());
        menuBar->addAction(menuExibir->menuAction());
        menuBar->addAction(menuAjuda->menuAction());
        menuArquivo->addSeparator();
        menuArquivo->addAction(actionNovo);
        menuArquivo->addAction(actionAbrir);
        menuArquivo->addAction(actionSalvar);
        menuArquivo->addAction(actionSalvar_Como);
        menuExibir->addAction(actionVisualizar_Imagens);
        menuEditar->addAction(actionBuscar_Padroes);
        menuEditar->addAction(actionCalibrar);
        menuAjuda->addAction(actionSobre);

        retranslateUi(StereoCalibClass);

        QMetaObject::connectSlotsByName(StereoCalibClass);
    } // setupUi

    void retranslateUi(QMainWindow *StereoCalibClass)
    {
        StereoCalibClass->setWindowTitle(QApplication::translate("StereoCalibClass", "StereoCalib", 0));
        actionAbrir->setText(QApplication::translate("StereoCalibClass", "Abrir", 0));
        actionSalvar->setText(QApplication::translate("StereoCalibClass", "Salvar", 0));
        actionNovo->setText(QApplication::translate("StereoCalibClass", "Novo", 0));
        actionSalvar_Como->setText(QApplication::translate("StereoCalibClass", "Salvar Como", 0));
        actionVisualizar_Imagens->setText(QApplication::translate("StereoCalibClass", "Visualizar Imagens", 0));
        actionCalibrar->setText(QApplication::translate("StereoCalibClass", "Calibrar", 0));
        actionSobre->setText(QApplication::translate("StereoCalibClass", "Sobre", 0));
        actionBuscar_Padroes->setText(QApplication::translate("StereoCalibClass", "Buscar Padr\303\265es", 0));
        groupBox->setTitle(QApplication::translate("StereoCalibClass", "Padr\303\243o", 0));
        pontosPorColunaLabel->setText(QApplication::translate("StereoCalibClass", "Pontos por linha", 0));
        label->setText(QApplication::translate("StereoCalibClass", "Pontos por coluna", 0));
        label_2->setText(QApplication::translate("StereoCalibClass", "Coordenada X origem (mm)", 0));
        label_3->setText(QApplication::translate("StereoCalibClass", "Coordenada Y origem (mm)", 0));
        l_padrao->setText(QString());
        distNciaLabel->setText(QApplication::translate("StereoCalibClass", "Dist\303\242ncia horizontal (mm)", 0));
        distNciaVerticalLabel->setText(QApplication::translate("StereoCalibClass", "Dist\303\242ncia vertical (mm)", 0));
        groupBox_2->setTitle(QApplication::translate("StereoCalibClass", "Imagens", 0));
#ifndef QT_NO_TOOLTIP
        bt_adiciona->setToolTip(QApplication::translate("StereoCalibClass", "Adicionar imagens", 0));
#endif // QT_NO_TOOLTIP
        bt_adiciona->setText(QApplication::translate("StereoCalibClass", "...", 0));
#ifndef QT_NO_TOOLTIP
        bt_remove->setToolTip(QApplication::translate("StereoCalibClass", "Remover imagens", 0));
#endif // QT_NO_TOOLTIP
        bt_remove->setText(QApplication::translate("StereoCalibClass", "...", 0));
#ifndef QT_NO_TOOLTIP
        bt_habilita->setToolTip(QApplication::translate("StereoCalibClass", "Marcar imagens", 0));
#endif // QT_NO_TOOLTIP
        bt_habilita->setText(QApplication::translate("StereoCalibClass", "...", 0));
#ifndef QT_NO_TOOLTIP
        bt_desabilita->setToolTip(QApplication::translate("StereoCalibClass", "Desmarcar imagens", 0));
#endif // QT_NO_TOOLTIP
        bt_desabilita->setText(QApplication::translate("StereoCalibClass", "...", 0));
#ifndef QT_NO_TOOLTIP
        bt_limpa->setToolTip(QApplication::translate("StereoCalibClass", "Limpar", 0));
#endif // QT_NO_TOOLTIP
        bt_limpa->setText(QApplication::translate("StereoCalibClass", "...", 0));
        menuArquivo->setTitle(QApplication::translate("StereoCalibClass", "Arquivo", 0));
        menuExibir->setTitle(QApplication::translate("StereoCalibClass", "Exibir", 0));
        menuEditar->setTitle(QApplication::translate("StereoCalibClass", "Editar", 0));
        menuAjuda->setTitle(QApplication::translate("StereoCalibClass", "Ajuda", 0));
    } // retranslateUi

};

namespace Ui {
    class StereoCalibClass: public Ui_StereoCalibClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STEREOCALIB_H
