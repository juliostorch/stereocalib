/****************************************************************************
** Meta object code from reading C++ file 'stereocalib.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../stereocalib.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'stereocalib.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_StereoCalib_t {
    QByteArrayData data[18];
    char stringdata[384];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_StereoCalib_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_StereoCalib_t qt_meta_stringdata_StereoCalib = {
    {
QT_MOC_LITERAL(0, 0, 11),
QT_MOC_LITERAL(1, 12, 22),
QT_MOC_LITERAL(2, 35, 0),
QT_MOC_LITERAL(3, 36, 20),
QT_MOC_LITERAL(4, 57, 22),
QT_MOC_LITERAL(5, 80, 24),
QT_MOC_LITERAL(6, 105, 19),
QT_MOC_LITERAL(7, 125, 35),
QT_MOC_LITERAL(8, 161, 7),
QT_MOC_LITERAL(9, 169, 29),
QT_MOC_LITERAL(10, 199, 7),
QT_MOC_LITERAL(11, 207, 8),
QT_MOC_LITERAL(12, 216, 33),
QT_MOC_LITERAL(13, 250, 27),
QT_MOC_LITERAL(14, 278, 25),
QT_MOC_LITERAL(15, 304, 30),
QT_MOC_LITERAL(16, 335, 24),
QT_MOC_LITERAL(17, 360, 23)
    },
    "StereoCalib\0on_bt_adiciona_clicked\0\0"
    "on_bt_remove_clicked\0on_bt_habilita_clicked\0"
    "on_bt_desabilita_clicked\0on_bt_limpa_clicked\0"
    "on_actionVisualizar_Imagens_toggled\0"
    "checked\0on_img_view_currentRowChanged\0"
    "current\0previous\0on_actionBuscar_Padroes_triggered\0"
    "on_actionCalibrar_triggered\0"
    "on_actionSalvar_triggered\0"
    "on_actionSalvar_Como_triggered\0"
    "on_actionAbrir_triggered\0"
    "on_actionNovo_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_StereoCalib[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x08 /* Private */,
       3,    0,   80,    2, 0x08 /* Private */,
       4,    0,   81,    2, 0x08 /* Private */,
       5,    0,   82,    2, 0x08 /* Private */,
       6,    0,   83,    2, 0x08 /* Private */,
       7,    1,   84,    2, 0x08 /* Private */,
       9,    2,   87,    2, 0x08 /* Private */,
      12,    0,   92,    2, 0x08 /* Private */,
      13,    0,   93,    2, 0x08 /* Private */,
      14,    0,   94,    2, 0x08 /* Private */,
      15,    0,   95,    2, 0x08 /* Private */,
      16,    0,   96,    2, 0x08 /* Private */,
      17,    0,   97,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::QModelIndex,   10,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void StereoCalib::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        StereoCalib *_t = static_cast<StereoCalib *>(_o);
        switch (_id) {
        case 0: _t->on_bt_adiciona_clicked(); break;
        case 1: _t->on_bt_remove_clicked(); break;
        case 2: _t->on_bt_habilita_clicked(); break;
        case 3: _t->on_bt_desabilita_clicked(); break;
        case 4: _t->on_bt_limpa_clicked(); break;
        case 5: _t->on_actionVisualizar_Imagens_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_img_view_currentRowChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 7: _t->on_actionBuscar_Padroes_triggered(); break;
        case 8: _t->on_actionCalibrar_triggered(); break;
        case 9: _t->on_actionSalvar_triggered(); break;
        case 10: _t->on_actionSalvar_Como_triggered(); break;
        case 11: _t->on_actionAbrir_triggered(); break;
        case 12: _t->on_actionNovo_triggered(); break;
        default: ;
        }
    }
}

const QMetaObject StereoCalib::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_StereoCalib.data,
      qt_meta_data_StereoCalib,  qt_static_metacall, 0, 0}
};


const QMetaObject *StereoCalib::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *StereoCalib::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_StereoCalib.stringdata))
        return static_cast<void*>(const_cast< StereoCalib*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int StereoCalib::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
struct qt_meta_stringdata_KeyFocusFilter_t {
    QByteArrayData data[1];
    char stringdata[15];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KeyFocusFilter_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KeyFocusFilter_t qt_meta_stringdata_KeyFocusFilter = {
    {
QT_MOC_LITERAL(0, 0, 14)
    },
    "KeyFocusFilter"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KeyFocusFilter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KeyFocusFilter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject KeyFocusFilter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_KeyFocusFilter.data,
      qt_meta_data_KeyFocusFilter,  qt_static_metacall, 0, 0}
};


const QMetaObject *KeyFocusFilter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KeyFocusFilter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KeyFocusFilter.stringdata))
        return static_cast<void*>(const_cast< KeyFocusFilter*>(this));
    return QObject::qt_metacast(_clname);
}

int KeyFocusFilter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
