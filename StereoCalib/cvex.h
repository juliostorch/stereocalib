#ifndef CVEX_H
#define CVEX_H

#include <opencv2/core/core.hpp>
#include <vector>
#include <ostream>
class QImage;
namespace cv {

    typedef Vec<double, 5> Vec5d;
    typedef Vec<double, 6> Vec6d;
    typedef Mat_<double> Mat_d;
    typedef Mat_<float> Mat_f;

    void convertPointsHomogeneous( const cv::Mat &src, cv::Mat &dst);
    void convertPointsHomogeneous3D( const cv::Mat &src, cv::Mat &dst);

//    template<typename _Tp, int n>
//    cv::Vec<_Tp, n> operator/( const cv::Vec<_Tp, n> &vec, double a )
//    {
//        cv::Vec<_Tp, n> r;
//        for( int i=0; i<n; i++ )
//            r[i] = vec[i]/a;
//        return r;
//    }


    //template<typename _Tp, int n>
    //std::ostream& operator<<( std::ostream& out, const cv::Vec<_Tp, n>& vec )
    //{
    //    out << '(';
    //    for( int i=0; i<n-1; i++)
    //        out << vec[i] << ", ";
    //    out << vec[n-1] << ")";

    //    return out;
    //}

    template<typename _Tp>
    cv::Mat_<_Tp> selectRows( const cv::Mat_<_Tp> &m, const std::vector<size_t> &idx ){
        cv::Mat_<_Tp> m2( idx.size(), m.cols );
        cv::Mat_<_Tp> m2i;

        for (size_t i=0; i<idx.size(); i++){
            m2i = m2.row( i );
            m.row( idx[i] ).copyTo( m2i );
        }
        return m2;
    }

    template<typename _Tp>
    std::ostream& operator<<( std::ostream& out, const std::vector<_Tp>& vec )
    {
        out << '[';
        for( size_t i=0; i<vec.size()-1; i++)
            out << vec[i] << ", ";
        out << vec[ vec.size()-1 ] << "];";

        return out;
    }

    template<typename _Tp>
    std::ostream& operator<<( std::ostream& out, const cv::Mat_<_Tp>& mat )
    {
        using namespace std;
        using namespace cv;

        //Encontra ordem de grandeza do maior elemento.
        int prec = 10;
        Mat aux = mat;
        aux = aux.reshape(1, 0);
        double minVal, maxVal;
        try{
            minMaxLoc(aux, &minVal, &maxVal);
        } catch(...) {
            minVal = maxVal = 1;
        }

        minVal = fabs( minVal );
        if (minVal > maxVal)
            maxVal = minVal;

        double exp = floor(log10(maxVal));
        double div = pow( 10, exp );

        ios_base::fmtflags flags = out.flags();
        out << fixed;
        out.precision(prec);

        if (exp != 0)
            out << "1e" << (int)exp << " * [" << endl;
        else
            out << '[' << endl;


        MatConstIterator_< _Tp > it = mat.begin();

        for (int i=0; i<mat.rows; i++){
            for (int j=0; j<mat.cols; j++, it++) {
                out.width(prec+6);
                out << (*it)/div;
            }
            out << ';' << endl;
        }
        out << "];" << endl;

        out.flags( flags );

        return out;
    }

    std::ostream& operator<<( std::ostream& out, const cv::Mat& mat );

	void operator<<(QImage& img, const cv::Mat& mat);
}

#endif // CVEX_H

