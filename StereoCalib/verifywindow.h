#ifndef VERIFYWINDOW_H
#define VERIFYWINDOW_H

#include <QDialog>
#include <qlabel.h>
#include <QVBoxLayout>
#include "core.h"

class QStandardItemModel;

namespace Ui {
	class verifyWindow;
}

class VerifyWindow : public QDialog
{
	Q_OBJECT

	QWidget *xlabel;
	QWidget *ylabel;
	QWidget *zlabel;
	QStringList imgNames;
	QStandardItemModel *imgModel;
	QStandardItemModel *selModel;
	QStandardItemModel *resultModel;
	QSortFilterProxyModel *sortModel;
	CalibPattern m_pattern;
	CalibData m_leftCalib;
	CalibData m_rightCalib;
	QList< std::vector<cv::Point2f> > *m_leftGrid;
	QList< std::vector<cv::Point2f> > *m_rightGrid;
	QList<  std::vector<cv::Point3f> > m_grids;
	QList<cv::Mat> m_rotation;
	QList<cv::Mat> m_translation;

public:
	VerifyWindow(QWidget *parent = 0);
	~VerifyWindow();
	void setUp(QStandardItemModel *model, const CalibPattern &pattern, const CalibData &left, const CalibData &right,
		QList< std::vector<cv::Point2f> > *leftGrids, QList< std::vector<cv::Point2f> > *rightGrids);

protected:
	void createErrorWindows();
	void recoverGrid3D(int pair_id);
	void calcMeanError(int pair_id, const std::vector<float> &realDist, float *mean, float *std) const;
	void addResultRow(int pair_id, float mean, float std);
	void showGridError(int pair_id);

protected slots:
	void on_btAdd_clicked();
	void on_btRemove_clicked();
	void on_btCalcular_clicked();
	void on_btExibirErro_clicked();

private:
	Ui::verifyWindow *ui;
};

class ErrorGraph : public QWidget{
	Q_OBJECT

		QLabel *graph;
	QLabel *rangeLabel;
	QVBoxLayout *layout;

public:
	ErrorGraph(QWidget *parent = 0) :QWidget(parent){
		layout = new QVBoxLayout;
		graph = new QLabel;
		rangeLabel = new QLabel;
		setLayout(layout);
		layout->addWidget(graph, 1);
		layout->addWidget(rangeLabel);
		graph->setScaledContents(true);
	}

	void setImage(const QImage &img){
		graph->setPixmap(QPixmap::fromImage(img));
	}
	void setRangeLabel(float min, float max){
		rangeLabel->setText(QString(QStringLiteral("M�nimo: %1\tM�ximo: %2")).arg(min).arg(max));
	}
	void setUp(const QImage &img, float min, float max){
		setImage(img);
		setRangeLabel(min, max);
		show();
	}
};

#endif //VERIFYWINDOW_H
