#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsView>

class View: public QGraphicsView
{
    Q_OBJECT

    bool mousePressed;
public:
    View(QWidget *parent = 0);
    ~View();

protected:
    void wheelEvent ( QWheelEvent * event );
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

signals:
    void clicked( QGraphicsItem* item, QPointF pos );
    void dragged( QPointF pos );
	void released( QGraphicsItem* item, QPointF pos );
};
#endif // VIEW_H
