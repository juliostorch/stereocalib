﻿#include "core.h"
#include <sba.h>
#include <iostream>
#include <fstream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "cvex.h"

using namespace cv;
using namespace std;

typedef struct {
    CalibData calib;
    cv::Mat points;
} Data;

typedef struct {
    Mat Rl;
    Mat Tl;
    Mat Rr;
    Mat Tr;

} PairData;


void singleCam_func(double *p, double *hx, int m, int n, void *adata){
    Data *data = static_cast<Data*>(adata) ;
    data->calib.cameraMatrix = (cv::Mat_<double>(3,3) << p[0], 0, p[2], 0, p[1], p[3], 0, 0, 1);
    data->calib.distortion = cv::Mat_<double>(m-4, 1, p+4);
    cv::Mat proj(n/2, 2, CV_64F, hx);
    data->calib.project( data->points ).copyTo(proj);
}

void singleCam_jac(double *p, double *j, int m, int, void *adata){
    Data *data = static_cast<Data*>(adata) ;
    data->calib.cameraMatrix = (cv::Mat_<double>(3,3) << p[0], 0, p[2], 0, p[1], p[3], 0, 0, 1);
    data->calib.distortion = cv::Mat_<double>(m-4, 1, p+4);
    cv::Mat jac(2, m+6, CV_64F);
    cv::Mat jac2(2, m, CV_64F, j);
    cv::Mat proj;

    data->calib.project( data->points, proj, jac);
    jac2 = jac.colRange(6, m+6);
}

void project( double *aj, double *bi, double *xij, const CalibData &calib){



}

void pair_func( int j, int /*i*/, double *aj, double *bi, double *xij, void *adata ){
    PairData *data = static_cast<PairData*>(adata) ;

    double *p=aj;
    double *F = p;
    double *Imc = p+2;
    double *D = p+4;
    double *P = bi;
    double *R;
    double *T;

    if (j==0){
        R = data->Rl.ptr<double>();
        T = data->Tl.ptr<double>();
    } else if (j==1){
        R = data->Rr.ptr<double>();
        T = data->Tr.ptr<double>();
    }


    double t249 = P[0]*R[0];
    double t250 = P[1]*R[1];
    double t251 = P[2]*R[2];
    double t252 = T[0]+t249+t250+t251;
    double t253 = P[0]*R[6];
    double t254 = P[1]*R[7];
    double t255 = P[2]*R[8];
    double t256 = T[2]+t253+t254+t255;
    double t257 = 1.0/(t256*t256);
    double t258 = P[0]*R[3];
    double t259 = P[1]*R[4];
    double t260 = P[2]*R[5];
    double t261 = T[1]+t258+t259+t260;
    double t262 = t252*t252;
    double t263 = t257*t262;
    double t264 = t261*t261;
    double t265 = t257*t264;
    double t266 = t263+t265;
    double t267 = t266*t266;
    double t268 = D[0]*t266;
    double t269 = D[1]*t267;
    double t270 = D[4]*t266*t267;
    double t271 = t268+t269+t270+1.0;
    double t272 = D[5]*t266;
    double t273 = D[6]*t267;
    double t274 = D[7]*t266*t267;
    double t275 = t272+t273+t274+1.0;
    double t276 = 1.0/t275;
    double t277 = 1.0/t256;
    xij[0] = Imc[0]+F[0]*(D[2]*t252*t257*t261*2.0+t252*t271*t276*t277);
    xij[1] = Imc[1]+F[1]*(D[3]*t252*t257*t261*2.0+t261*t271*t276*t277);

}

void pair_jac( int j, int /*i*/, double *aj, double *bi, double *Aij, double *Bij, void *adata ){
    PairData *data = static_cast<PairData*>(adata) ;

    double *p=aj;
    double *F = p;
    double *D = p+4;
    double *P = bi;
    double *R;
    double *T;

    if (j==0){
        R = data->Rl.ptr<double>();
        T = data->Tl.ptr<double>();
    } else if (j==1){
        R = data->Rr.ptr<double>();
        T = data->Tr.ptr<double>();
    }

//    double t112 = P[0]*R[0];
//    double t113 = P[1]*R[1];
//    double t114 = P[2]*R[2];
//    double t105 = T[0]+t112+t113+t114;
//    double t115 = P[0]*R[3];
//    double t116 = P[1]*R[4];
//    double t117 = P[2]*R[5];
//    double t106 = T[1]+t115+t116+t117;
//    double t107 = P[0]*R[6];
//    double t108 = P[1]*R[7];
//    double t109 = P[2]*R[8];
//    double t110 = T[2]+t107+t108+t109;
//    double t111 = 1.0/(t110*t110);
//    double t118 = t105*t105;
//    double t119 = t106*t106;
//    double t120 = t111*t119;
//    double t121 = t111*t118;
//    double t122 = t120+t121;
//    double t123 = t122*t122;
//    double t124 = D[5]*t122;
//    double t125 = D[6]*t123;
//    double t126 = D[7]*t122*t123;
//    double t127 = t124+t125+t126+1.0;
//    double t128 = 1.0/t127;
//    double t129 = 1.0/t110;
//    double t130 = t111*t118*3.0;
//    double t131 = t120+t130;
//    double t132 = D[0]*t122;
//    double t133 = D[1]*t123;
//    double t134 = D[4]*t122*t123;
//    double t135 = t132+t133+t134+1.0;
//    double t136 = 1.0/(t127*t127);
//    double t137 = 1.0/(t110*t110*t110);
//    double t138 = R[3]*t106*t111*2.0;
//    double t139 = R[0]*t105*t111*2.0;
//    double t141 = R[6]*t118*t137*2.0;
//    double t142 = R[6]*t119*t137*2.0;
//    double t140 = t138+t139-t141-t142;
//    double t143 = R[4]*t106*t111*2.0;
//    double t144 = R[1]*t105*t111*2.0;
//    double t146 = R[7]*t118*t137*2.0;
//    double t147 = R[7]*t119*t137*2.0;
//    double t145 = t143+t144-t146-t147;
//    double t148 = R[5]*t106*t111*2.0;
//    double t149 = R[2]*t105*t111*2.0;
//    double t151 = R[8]*t118*t137*2.0;
//    double t152 = R[8]*t119*t137*2.0;
//    double t150 = t148+t149-t151-t152;
//    double t153 = t111*t119*3.0;
//    double t154 = t121+t153;
//    double t155 = D[0]*t140;
//    double t156 = D[1]*t122*t140*2.0;
//    double t157 = D[4]*t123*t140*3.0;
//    double t158 = t155+t156+t157;
//    double t159 = D[5]*t140;
//    double t160 = D[6]*t122*t140*2.0;
//    double t161 = D[7]*t123*t140*3.0;
//    double t162 = t159+t160+t161;
//    double t163 = D[0]*t145;
//    double t164 = D[1]*t122*t145*2.0;
//    double t165 = D[4]*t123*t145*3.0;
//    double t166 = t163+t164+t165;
//    double t167 = D[5]*t145;
//    double t168 = D[6]*t122*t145*2.0;
//    double t169 = D[7]*t123*t145*3.0;
//    double t170 = t167+t168+t169;
//    double t171 = D[0]*t150;
//    double t172 = D[1]*t122*t150*2.0;
//    double t173 = D[4]*t123*t150*3.0;
//    double t174 = t171+t172+t173;
//    double t175 = D[5]*t150;
//    double t176 = D[6]*t122*t150*2.0;
//    double t177 = D[7]*t123*t150*3.0;
//    double t178 = t175+t176+t177;

    double t279 = P[0]*R[0];
    double t280 = P[1]*R[1];
    double t281 = P[2]*R[2];
    double t282 = T[0]+t279+t280+t281;
    double t283 = P[0]*R[6];
    double t284 = P[1]*R[7];
    double t285 = P[2]*R[8];
    double t286 = T[2]+t283+t284+t285;
    double t287 = 1.0/(t286*t286);
    double t288 = P[0]*R[3];
    double t289 = P[1]*R[4];
    double t290 = P[2]*R[5];
    double t291 = T[1]+t288+t289+t290;
    double t292 = t282*t282;
    double t293 = t287*t292;
    double t294 = t291*t291;
    double t295 = t287*t294;
    double t296 = t293+t295;
    double t297 = t296*t296;
    double t298 = D[5]*t296;
    double t299 = D[6]*t297;
    double t300 = D[7]*t296*t297;
    double t301 = t298+t299+t300+1.0;
    double t302 = 1.0/t301;
    double t303 = 1.0/t286;
    double t304 = D[0]*t296;
    double t305 = D[1]*t297;
    double t306 = D[4]*t296*t297;
    double t307 = t304+t305+t306+1.0;
    double t308 = 1.0/(t301*t301);
    double t309 = 1.0/(t286*t286*t286);
    double t310 = R[0]*t282*t287*2.0;
    double t311 = R[3]*t287*t291*2.0;
    double t313 = R[6]*t292*t309*2.0;
    double t314 = R[6]*t294*t309*2.0;
    double t312 = t310+t311-t313-t314;
    double t315 = R[1]*t282*t287*2.0;
    double t316 = R[4]*t287*t291*2.0;
    double t318 = R[7]*t292*t309*2.0;
    double t319 = R[7]*t294*t309*2.0;
    double t317 = t315+t316-t318-t319;
    double t320 = R[2]*t282*t287*2.0;
    double t321 = R[5]*t287*t291*2.0;
    double t323 = R[8]*t292*t309*2.0;
    double t324 = R[8]*t294*t309*2.0;
    double t322 = t320+t321-t323-t324;
    double t325 = D[0]*t312;
    double t326 = D[1]*t296*t312*2.0;
    double t327 = D[4]*t297*t312*3.0;
    double t328 = t325+t326+t327;
    double t329 = D[5]*t312;
    double t330 = D[6]*t296*t312*2.0;
    double t331 = D[7]*t297*t312*3.0;
    double t332 = t329+t330+t331;
    double t333 = D[0]*t317;
    double t334 = D[1]*t296*t317*2.0;
    double t335 = D[4]*t297*t317*3.0;
    double t336 = t333+t334+t335;
    double t337 = D[5]*t317;
    double t338 = D[6]*t296*t317*2.0;
    double t339 = D[7]*t297*t317*3.0;
    double t340 = t337+t338+t339;
    double t341 = D[0]*t322;
    double t342 = D[1]*t296*t322*2.0;
    double t343 = D[4]*t297*t322*3.0;
    double t344 = t341+t342+t343;
    double t345 = D[5]*t322;
    double t346 = D[6]*t296*t322*2.0;
    double t347 = D[7]*t297*t322*3.0;
    double t348 = t345+t346+t347;

    Mat jacMat;
    jacMat=Mat::zeros(2, 15, CV_64F);
    double *jac = jacMat.ptr<double>();


//    jac[0] = D[3]*t131+D[2]*t105*t106*t111*2.0+t105*t128*t129*t135;
//    jac[2] = 1.0;
//    jac[4] = F[0]*t105*t122*t128*t129;
//    jac[5] = F[0]*t105*t123*t128*t129;
//    jac[6] = F[0]*t105*t106*t111*2.0;
//    jac[7] = F[0]*t131;
//    jac[8] = F[0]*t105*t122*t123*t128*t129;
//    jac[9] = -F[0]*t105*t122*t129*t135*t136;
//    jac[10] = -F[0]*t105*t123*t129*t135*t136;
//    jac[11] = -F[0]*t105*t122*t123*t129*t135*t136;
//    jac[12] = F[0]*(D[3]*(t138+R[0]*t105*t111*6.0-R[6]*t118*t137*6.0-R[6]*t119*t137*2.0)+R[0]*t128*t129*t135+t105*t128*t129*t158+D[2]*R[0]*t106*t111*2.0+D[2]*R[3]*t105*t111*2.0-D[2]*R[6]*t105*t106*t137*4.0-R[6]*t105*t111*t128*t135-t105*t129*t135*t136*t162);
//    jac[13] = F[0]*(D[3]*(t143+R[1]*t105*t111*6.0-R[7]*t118*t137*6.0-R[7]*t119*t137*2.0)+R[1]*t128*t129*t135+t105*t128*t129*t166+D[2]*R[1]*t106*t111*2.0+D[2]*R[4]*t105*t111*2.0-D[2]*R[7]*t105*t106*t137*4.0-R[7]*t105*t111*t128*t135-t105*t129*t135*t136*t170);
//    jac[14] = F[0]*(D[3]*(t148+R[2]*t105*t111*6.0-R[8]*t118*t137*6.0-R[8]*t119*t137*2.0)+R[2]*t128*t129*t135+t105*t128*t129*t174+D[2]*R[2]*t106*t111*2.0+D[2]*R[5]*t105*t111*2.0-D[2]*R[8]*t105*t106*t137*4.0-R[8]*t105*t111*t128*t135-t105*t129*t135*t136*t178);
//    jac[16] = D[2]*t154+D[3]*t105*t106*t111*2.0+t106*t128*t129*t135;
//    jac[18] = 1.0;
//    jac[19] = F[1]*t106*t122*t128*t129;
//    jac[20] = F[1]*t106*t123*t128*t129;
//    jac[21] = F[1]*t154;
//    jac[22] = F[1]*t105*t106*t111*2.0;
//    jac[23] = F[1]*t106*t122*t123*t128*t129;
//    jac[24] = -F[1]*t106*t122*t129*t135*t136;
//    jac[25] = -F[1]*t106*t123*t129*t135*t136;
//    jac[26] = -F[1]*t106*t122*t123*t129*t135*t136;
//    jac[27] = F[1]*(D[2]*(t139-t141+R[3]*t106*t111*6.0-R[6]*t119*t137*6.0)+R[3]*t128*t129*t135+t106*t128*t129*t158+D[3]*R[0]*t106*t111*2.0+D[3]*R[3]*t105*t111*2.0-D[3]*R[6]*t105*t106*t137*4.0-R[6]*t106*t111*t128*t135-t106*t129*t135*t136*t162);
//    jac[28] = F[1]*(D[2]*(t144-t146+R[4]*t106*t111*6.0-R[7]*t119*t137*6.0)+R[4]*t128*t129*t135+t106*t128*t129*t166+D[3]*R[1]*t106*t111*2.0+D[3]*R[4]*t105*t111*2.0-D[3]*R[7]*t105*t106*t137*4.0-R[7]*t106*t111*t128*t135-t106*t129*t135*t136*t170);
//    jac[29] = F[1]*(D[2]*(t149-t151+R[5]*t106*t111*6.0-R[8]*t119*t137*6.0)+R[5]*t128*t129*t135+t106*t128*t129*t174+D[3]*R[2]*t106*t111*2.0+D[3]*R[5]*t105*t111*2.0-D[3]*R[8]*t105*t106*t137*4.0-R[8]*t106*t111*t128*t135-t106*t129*t135*t136*t178);


    jac[0] = D[2]*t282*t287*t291*2.0+t282*t302*t303*t307;
    jac[2] = 1.0;
    jac[4] = F[0]*t282*t296*t302*t303;
    jac[5] = F[0]*t282*t297*t302*t303;
    jac[6] = F[0]*t282*t287*t291*2.0;
    jac[8] = F[0]*t282*t296*t297*t302*t303;
    jac[9] = -F[0]*t282*t296*t303*t307*t308;
    jac[10] = -F[0]*t282*t297*t303*t307*t308;
    jac[11] = -F[0]*t282*t296*t297*t303*t307*t308;
    jac[12] = F[0]*(R[0]*t302*t303*t307+t282*t302*t303*t328+D[2]*R[0]*t287*t291*2.0+D[2]*R[3]*t282*t287*2.0-D[2]*R[6]*t282*t291*t309*4.0-R[6]*t282*t287*t302*t307-t282*t303*t307*t308*t332);
    jac[13] = F[0]*(R[1]*t302*t303*t307+t282*t302*t303*t336+D[2]*R[1]*t287*t291*2.0+D[2]*R[4]*t282*t287*2.0-D[2]*R[7]*t282*t291*t309*4.0-R[7]*t282*t287*t302*t307-t282*t303*t307*t308*t340);
    jac[14] = F[0]*(R[2]*t302*t303*t307+t282*t302*t303*t344+D[2]*R[2]*t287*t291*2.0+D[2]*R[5]*t282*t287*2.0-D[2]*R[8]*t282*t291*t309*4.0-R[8]*t282*t287*t302*t307-t282*t303*t307*t308*t348);
    jac[16] = D[3]*t282*t287*t291*2.0+t291*t302*t303*t307;
    jac[18] = 1.0;
    jac[19] = F[1]*t291*t296*t302*t303;
    jac[20] = F[1]*t291*t297*t302*t303;
    jac[22] = F[1]*t282*t287*t291*2.0;
    jac[23] = F[1]*t291*t296*t297*t302*t303;
    jac[24] = -F[1]*t291*t296*t303*t307*t308;
    jac[25] = -F[1]*t291*t297*t303*t307*t308;
    jac[26] = -F[1]*t291*t296*t297*t303*t307*t308;
    jac[27] = F[1]*(R[3]*t302*t303*t307+t291*t302*t303*t328+D[3]*R[0]*t287*t291*2.0+D[3]*R[3]*t282*t287*2.0-D[3]*R[6]*t282*t291*t309*4.0-R[6]*t287*t291*t302*t307-t291*t303*t307*t308*t332);
    jac[28] = F[1]*(R[4]*t302*t303*t307+t291*t302*t303*t336+D[3]*R[1]*t287*t291*2.0+D[3]*R[4]*t282*t287*2.0-D[3]*R[7]*t282*t291*t309*4.0-R[7]*t287*t291*t302*t307-t291*t303*t307*t308*t340);
    jac[29] = F[1]*(R[5]*t302*t303*t307+t291*t302*t303*t344+D[3]*R[2]*t287*t291*2.0+D[3]*R[5]*t282*t287*2.0-D[3]*R[8]*t282*t291*t309*4.0-R[8]*t287*t291*t302*t307-t291*t303*t307*t308*t348);

    Mat jac_A(2, 12, CV_64F, Aij);
    Mat jac_B(2, 3, CV_64F, Bij);
    Mat aux;

    aux = jacMat(Range(0, 2), Range(0, 12));
    aux.copyTo(jac_A);
//    jac_A=0.;
//    jac_A.colRange(8,12)=0.;
    aux = jacMat(Range(0, 2), Range(12, 15));
    aux.copyTo(jac_B);

}


std::vector<cv::Point3f> triangulate(const CalibData &leftCalib, const CalibData &rightCalib, const Pair &pair){
    vector<Point3f> points;
    points = triangulate(leftCalib, rightCalib, pair.leftCenters, pair.rightCenters);

    Mat_<double> leftPointsMat = Mat(pair.leftCenters).reshape(1);
    Mat_<double> rightPointsMat = Mat(pair.rightCenters).reshape(1);
    Mat_<double> pointsMat = Mat(points).reshape(1,1);


    int numCam=2;
    int numPoints=points.size();
    int numPar = 12*numCam+3*numPoints;

    Mat_<double> param(1, numPar);
    Mat_<double> cam = param.colRange(0, 12*numCam);
    Mat_<double> str = param.colRange(12*numCam, numPar);
    Mat_<double> proj(numPoints, 2*numCam);

    Mat aux = cam.colRange(0, 12);
    leftCalib.getIntrinsicCompact().copyTo(aux);
    aux = cam.colRange(12, 24);
    rightCalib.getIntrinsicCompact().copyTo(aux);

    pointsMat.copyTo(str);

    aux = proj.colRange(0, 2);
    leftPointsMat.copyTo(aux);
    aux = proj.colRange(2, 4);
    rightPointsMat.copyTo(aux);

    Mat mask = Mat::ones(numPoints, numCam, CV_8S);

    PairData adata;
    adata.Rl = (Mat_<double>)leftCalib.rotationMatrix;
    adata.Tl = (Mat_<double>)leftCalib.translationMatrix;
    adata.Rr = (Mat_<double>)rightCalib.rotationMatrix;
    adata.Tr = (Mat_<double>)rightCalib.translationMatrix;


    double opts[] = {1e-3, 1e-20, 1e-20, 1e-20, 0, 0, 0 };
    double _info[20] = {0};


    sba_motstr_levmar( numPoints, 0, numCam, 0, mask.ptr<char>(), param.ptr<double>(), 12, 3, proj.ptr<double>(), NULL, 2,
                      pair_func, pair_jac, (void*)&adata, 500, 0, opts, _info );

    return str.reshape(3);

}

std::vector<std::vector<cv::Point3f> > triangulate(const CalibData &leftCalib, const CalibData &rightCalib, const std::vector<Pair> &pairs, bool adjust){
    std::vector<cv::Point3f> points;
    std::vector<std::vector<cv::Point3f> > final;
    Pair pair;

    vector<size_t> start;
    for(size_t i=0; i<pairs.size(); i++){
        start.push_back( pair.leftCenters.size() );
        pair.leftCenters << pairs[i].leftCenters;
        pair.rightCenters << pairs[i].rightCenters;
    }

    points = triangulate( leftCalib, rightCalib, pair.leftCenters, pair.rightCenters );

    final.resize(pairs.size());
    for(size_t i=0; i<pairs.size()-1;i++){
        final[i].assign( points.begin()+start[i], points.begin()+start[i+1] );
    }
    final.back().assign( points.begin()+start.back(), points.end() );

    return final;

}

void estimateRigidMotion( const std::vector<cv::Point3f> &_x, const std::vector<cv::Point3f> &_y, cv::Mat &R, cv::Mat &T){
    assert(_x.size() == _y.size());


    Mat_d x = (Mat_f) ((Mat) _x).reshape(1);
    Mat_d y = (Mat_f) ((Mat) _y).reshape(1);
//    Mat_d y = Mat_f(_y.size(), 3, &_y[0].x);


    Mat cx, cy;
    reduce(x, cx, 0, CV_REDUCE_AVG);
    reduce(y, cy, 0, CV_REDUCE_AVG);

    Mat_d xcentralized = x-Mat_d::ones(x.rows, 1)*cx;
    Mat_d ycentralized = y-Mat_d::ones(y.rows, 1)*cy;
    Mat_d H = xcentralized.t()*ycentralized;

    SVD svd;
    svd(H, SVD::MODIFY_A | SVD::FULL_UV);

    R = svd.vt.t()*svd.u;
    T = cy.t() - R*cx.t();
}

void recoverPatternPose( std::vector<Pair> &pairs, CalibData &leftCalib, CalibData &rightCalib ){
    if (!leftCalib.isValid() || !rightCalib.isValid())
        return;

//    for(int i=pairs.size()-1; i>=0; i--)
//        if (!pairs[i].isValid())
//            pairs.erase( pairs.begin() +i);

//    std::vector<std::vector<cv::Point3f> > points = triangulate(leftCalib, rightCalib, pairs, false);

//    for(size_t i=0; i<pairs.size(); i++){
//        cv::Mat R, T;

//        estimateRigidMotion( pairs[i].pattern.getPatternCenters(), points[i], R, T );
//        pairs[i].rotationMatrix = R;
//        pairs[i].translationMatrix = T;
//    }


    for(size_t i=0; i<pairs.size(); i++){
        if (pairs[i].isValid()){
            Mat R, T;
            vector<Point3f> pt = triangulate(leftCalib, rightCalib, pairs[i].leftCenters, pairs[i].rightCenters);
            estimateRigidMotion(pairs[i].pattern.getPatternCenters(), pt, R, T);
            pairs[i].rotationMatrix = R;
            pairs[i].translationMatrix = T;
        }
    }
}

std::vector<Pair> generatePairs( const std::vector<Pair> &ref, const CalibData &leftCalib, const CalibData &rightCalib, float gridError, float cameraError ){
    RNG rng(getTickCount());

    std::vector<Pair> gen;
    for(size_t i=0; i<ref.size();i++){
        if (ref[i].isValid() && !ref[i].rotationMatrix.empty() && !ref[i].translationMatrix.empty()){
            Mat points = ((Mat)ref[i].pattern.getPatternCenters( ref[i].rotationMatrix, ref[i].translationMatrix )).reshape(1).clone();
            Mat noise(points.rows, 3, CV_32FC1);
            float ge = gridError*ref[i].pattern.xDistance/sqrt(3);
//            rng.fill( noise, RNG::UNIFORM, -ge, ge);
            rng.fill( noise, RNG::UNIFORM, -gridError/sqrt(3), gridError/sqrt(3));
//            rng.fill( noise, RNG::NORMAL, 0, gridError/sqrt(3));
            points += noise;

            Mat leftCenters = leftCalib.project(points).reshape(1);
            Mat rightCenters = rightCalib.project(points).reshape(1);
            noise = Mat(leftCenters.rows, 2, CV_32FC1);
            rng.fill( noise, RNG::UNIFORM, -cameraError, cameraError );
//            rng.fill( noise, RNG::NORMAL, 0, cameraError );
            leftCenters += noise;
            rng.fill( noise, RNG::UNIFORM, -cameraError, cameraError );
//            rng.fill( noise, RNG::NORMAL, 0, cameraError );
            rightCenters += noise;

            Pair p = ref[i];
            Mat aux = ((Mat)p.leftCenters).reshape(1);
//            leftCenters = ((Mat)ref[i].leftCenters).reshape(1);
            leftCenters.copyTo(aux);
            aux = ((Mat)p.rightCenters).reshape(1);

//            rightCenters = ((Mat)ref[i].rightCenters).reshape(1);
            rightCenters.copyTo(aux);
            gen.push_back(p);
        }
    }
    return gen;
}

void refinePair( CalibData &leftCalib, CalibData &rightCalib, const vector<Point2f> &leftPoints, const vector<Point2f> &rightPoints ){
    vector<Point3f> points;
    points = triangulate(leftCalib, rightCalib, leftPoints, rightPoints);

    Mat_<double> leftPointsMat = Mat(leftPoints).reshape(1);
    Mat_<double> rightPointsMat = Mat(rightPoints).reshape(1);
    Mat_<double> pointsMat = Mat(points).reshape(1,1);


    int numCam=2;
    int numPoints=points.size();
    int numPar = 12*numCam+3*numPoints;

    Mat_<double> param(1, numPar);
    Mat_<double> cam = param.colRange(0, 12*numCam);
    Mat_<double> str = param.colRange(12*numCam, numPar);
    Mat_<double> proj(numPoints, 2*numCam);

    Mat aux = cam.colRange(0, 12);
    leftCalib.getIntrinsicCompact().copyTo(aux);
    aux = cam.colRange(12, 24);
    rightCalib.getIntrinsicCompact().copyTo(aux);

    pointsMat.copyTo(str);

    aux = proj.colRange(0, 2);
    leftPointsMat.copyTo(aux);
    aux = proj.colRange(2, 4);
    rightPointsMat.copyTo(aux);

    Mat mask = Mat::ones(numPoints, numCam, CV_8S);

    PairData adata;
    adata.Rl = (Mat_<double>)leftCalib.rotationMatrix;
    adata.Tl = (Mat_<double>)leftCalib.translationMatrix;
    adata.Rr = (Mat_<double>)rightCalib.rotationMatrix;
    adata.Tr = (Mat_<double>)rightCalib.translationMatrix;


    double opts[] = {1e-3, 1e-20, 1e-20, 1e-20, 0, 0, 0 };
    double _info[20] = {0};

    fstream arq;
    arq.open("teste.m", ios_base::out);
    arq << "left=" << leftCalib.project(Mat(points)) << endl;
    arq << "right=" << rightCalib.project(Mat(points)) << endl;
    arq << "param0=" << cam << ";" << endl;
    arq << "points0=" << str.reshape(1, points.size()) << ";" << endl;

    sba_motstr_levmar( numPoints, 0, numCam, 0, mask.ptr<char>(), param.ptr<double>(), 12, 3, proj.ptr<double>(), NULL, 2,
                      pair_func, pair_jac, (void*)&adata, 500, 0, opts, _info );

    arq << "leftpoints=" << leftPointsMat << ";" << endl;
    arq << "rightpoints=" << rightPointsMat << ";" << endl;
    arq << "proj=" << proj << ";" << endl;
    arq << "points=" << str.reshape(1, points.size()) << ";" << endl;
    arq << "param=" << cam << ";" << endl;
    arq << "Rl=" << adata.Rl << endl;
    arq << "Tl=" << adata.Tl << endl;
    arq << "Rr=" << adata.Rr << endl;
    arq << "Tr=" << adata.Tr << endl;

//    projectPoints(pointsMat, Mat::zeros(1, 3), Mat::zeros(1, 3), leftCalib.cameraMatrix, camer);


    leftCalib.setIntrinsicCompact(param.colRange(0, 12));
    rightCalib.setIntrinsicCompact(param.colRange(12, 24));

}


void refinePair( CalibData &leftCalib, CalibData &rightCalib, const vector<Point2f> &leftPoints, const vector<Point2f> &rightPoints, const vector<Point3f> &points ){

    Mat_<double> leftPointsMat = Mat(leftPoints).reshape(1);
    Mat_<double> rightPointsMat = Mat(rightPoints).reshape(1);
    Mat_<double> pointsMat = Mat(points).reshape(1,1);

    int numCam=2;
    int numPoints=points.size();
    int numPar = 12*numCam+3*numPoints;

    Mat_<double> param(1, numPar);
    Mat_<double> cam = param.colRange(0, 12*numCam);
    Mat_<double> str = param.colRange(12*numCam, numPar);
    Mat_<double> proj(numPoints, 2*numCam);

    Mat aux = cam.colRange(0, 12);
    leftCalib.getIntrinsicCompact().copyTo(aux);
    aux = cam.colRange(12, 24);
    rightCalib.getIntrinsicCompact().copyTo(aux);

    pointsMat.copyTo(str);

    aux = proj.colRange(0, 2);
    leftPointsMat.copyTo(aux);
    aux = proj.colRange(2, 4);
    rightPointsMat.copyTo(aux);

    Mat mask = Mat::ones(numPoints, numCam, CV_8S);

    PairData adata;
    adata.Rl = (Mat_<double>)leftCalib.rotationMatrix;
    adata.Tl = (Mat_<double>)leftCalib.translationMatrix;
    adata.Rr = (Mat_<double>)rightCalib.rotationMatrix;
    adata.Tr = (Mat_<double>)rightCalib.translationMatrix;


    double opts[] = {1e-3, 1e-12, 1e-12, 1e-12, 0, 0, 0 };
    double _info[20] = {0};

    fstream arq;
    arq.open("teste.m", ios_base::out);
    arq << "param=" << param << ";" << endl;
    arq << "points0=" << points << ";" << endl;

    sba_motstr_levmar( numPoints, numPoints, numCam, 0, mask.ptr<char>(), param.ptr<double>(), 12, 3, proj.ptr<double>(), NULL, 2,
                      pair_func, pair_jac, (void*)&adata, 500, 0, opts, _info );

    arq << "leftpoints=" << leftPointsMat << ";" << endl;
    arq << "rightpoints=" << rightPointsMat << ";" << endl;
    arq << "proj=" << proj << ";" << endl;
    arq << "points=" << points << ";" << endl;
    arq << "param=" << param << ";" << endl;


    leftCalib.setIntrinsicCompact(param.colRange(0, 12));
    rightCalib.setIntrinsicCompact(param.colRange(12, 24));

//    Mat_<double> leftPointsMat = Mat(leftPoints).reshape(1);
//    Mat_<double> rightPointsMat = Mat(rightPoints).reshape(1);
//    Mat_<double> pointsMat = Mat(points).reshape(1);


//    int numCam=2;
//    int numPoints=points.size();
//    int numPar = 12*numCam;

//    Mat_<double> param(1, numPar);
//    Mat_<double> proj(numPoints, 2*numCam);

//    Mat aux = param.colRange(0, 12);
//    leftCalib.getIntrinsicCompact().copyTo(aux);
//    aux = param.colRange(12, 24);
//    rightCalib.getIntrinsicCompact().copyTo(aux);

//    aux = proj.colRange(0, 2);
//    leftPointsMat.copyTo(aux);
//    aux = proj.colRange(2, 4);
//    rightPointsMat.copyTo(aux);

//    Mat mask = Mat::ones(numPoints, numCam, CV_8S);

//    PairData adata;
//    adata.Rl = (Mat_<double>)leftCalib.rotationMatrix;
//    adata.Tl = (Mat_<double>)leftCalib.translationMatrix;
//    adata.Rr = (Mat_<double>)rightCalib.rotationMatrix;
//    adata.Tr = (Mat_<double>)rightCalib.translationMatrix;
//    adata.P = (Mat_<double>)points;


//    double opts[] = {1e-3, 1e-12, 1e-12, 1e-12, 0, 0, 0 };
//    double _info[20] = {0};

//    sba_mot_levmar( numPoints, numCam, 0, mask.ptr<char>(), param.ptr<double>(), 12, 3, proj.ptr<double>(), NULL, 2,
//                      pair_func, pair_jac, (void*)&adata, 500, 0, opts, _info );

//    fstream arq;
//    arq.open("teste.m", ios_base::out);
//    arq.open("teste.m", ios_base::out);
//    arq << "leftpoints=" << leftPointsMat << ";" << endl;
//    arq << "rightpoints=" << rightPointsMat << ";" << endl;
//    arq << "proj=" << proj << ";" << endl;
//    arq << "points=" << points << ";" << endl;
//    arq << "param=" << param << ";" << endl;


//    leftCalib.setIntrinsicCompact(param.colRange(0, 12));
//    rightCalib.setIntrinsicCompact(param.colRange(12, 24));

}


void refineCam(CalibData &calib, vector<Point3f> objPoints, vector<Point2f> imagePoints){
    Data data;
    data.calib = calib;
    data.points = (Mat_<double>)Mat(objPoints).reshape(1, objPoints.size());

    double param[12];
    if (calib.cameraMatrix.type() == CV_32F){
        param[0] = calib.cameraMatrix.at<float>(0,0);
        param[1] = calib.cameraMatrix.at<float>(1,1);
        param[2] = calib.cameraMatrix.at<float>(0,2);
        param[3] = calib.cameraMatrix.at<float>(1,2);
    } else if (calib.cameraMatrix.type() == CV_64F){
        param[0] = calib.cameraMatrix.at<double>(0,0);
        param[1] = calib.cameraMatrix.at<double>(1,1);
        param[2] = calib.cameraMatrix.at<double>(0,2);
        param[3] = calib.cameraMatrix.at<double>(1,2);
    }

    Mat_<double> dist(8, 1, param+4);
    calib.distortion.copyTo(dist);

    Mat_<double> proj = Mat(imagePoints).reshape(1, imagePoints.size());

    double opts[] = {1e-3, 1e-20, 1e-12, 1e-12, 1e-12};
    double info[10];

    dlevmar_der(singleCam_func, singleCam_jac, param, (double*)proj.data, 12, imagePoints.size(), 100, opts, info, 0, 0, &data);

    calib.cameraMatrix = (cv::Mat_<double>(3,3) << param[0], 0, param[2], 0, param[1], param[3], 0, 0, 1);
    calib.distortion = cv::Mat_<double>(8, 1, param+4);
    calib.reprojectionError = info[1]/imagePoints.size();

}


