#ifndef STEREOCALIB_H
#define STEREOCALIB_H

#include <QtWidgets/QMainWindow>
#include "ui_stereocalib.h"
#include <qstandarditemmodel.h>
#include "core.h"

class StereoCalib : public QMainWindow
{
	Q_OBJECT

	QString m_filename;
	typedef std::vector < cv::Point2f >  GridPoints;
	QList<GridPoints> m_leftGrid;
	QList<GridPoints> m_rightGrid;
	QStandardItemModel *model_img;
	CalibData m_leftCalib;
	CalibData m_rightCalib;
	

public:
	StereoCalib(QWidget *parent = 0);
	~StereoCalib();

	CalibPattern calibPattern() const;
	void setCalibPattern(const CalibPattern &pattern);
	void clear();

protected:
	void loadImages(const QStringList &files);
	bool loadThumbnail(const QString &filename);
	void findGridInPair(int id);
	void drawGridInPairImage(int id);
	bool save(const QString &filename);
	bool load(const QString &filename);

private slots:
	void on_bt_adiciona_clicked();
	void on_bt_remove_clicked();
	void on_bt_habilita_clicked();
	void on_bt_desabilita_clicked();
	void on_bt_limpa_clicked();
	void on_actionVisualizar_Imagens_toggled(bool checked);
	void on_img_view_currentRowChanged(const QModelIndex & current, const QModelIndex & previous);
	void on_actionBuscar_Padroes_triggered();
	void on_actionCalibrar_triggered();
	void on_actionSalvar_triggered();
	void on_actionSalvar_Como_triggered();
	void on_actionAbrir_triggered();
	void on_actionNovo_triggered();
	void on_actionExportar_para_SVE_triggered();
	void on_actionMedir_triggered();
	void on_actionAferir_triggered();

private:
	Ui::StereoCalibClass ui;

};

class KeyFocusFilter : public QObject{
	Q_OBJECT

private:
	QLabel *padrao;

public:
	KeyFocusFilter(QObject *parent) :
		QObject(parent){}
	~KeyFocusFilter(){}
	void setLabel(QLabel *l){ padrao = l; }


protected:
	bool eventFilter(QObject *obj, QEvent *event){
		if (event->type() == QEvent::FocusIn)
			padrao->setPixmap(QPixmap(QString(":/%1").arg(obj->objectName())));
		return false;
	}
};

#endif // STEREOCALIB_H
