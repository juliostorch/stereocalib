#include "measurewindow.h"
#include "ui_measurewindow.h"
#include <QtGui>


PointsModel::PointsModel(QObject * parent)
    :QAbstractTableModel(parent){

}

PointsModel::~PointsModel(){

}

int PointsModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;
    else
        return leftPoints.size();


}
int PointsModel::columnCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;
    else
        return 7;

}
QVariant PointsModel::data(const QModelIndex &index, int role) const{
    if (index.parent().isValid() || role!=Qt::DisplayRole)
        return QVariant();

    int i = index.row();
    switch (index.column()){
    case 0: return leftPoints[i].x();
    case 1: return leftPoints[i].y();
    case 2: return rightPoints[i].x();
    case 3: return rightPoints[i].y();
    case 4: return triangulatedPoints[i].x;
    case 5: return triangulatedPoints[i].y;
    case 6: return triangulatedPoints[i].z;
    default: return QVariant();
    }

    return QVariant();
}

QVariant  PointsModel::headerData ( int section, Qt::Orientation orientation, int role ) const{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole){
        switch (section){
        case 0: return "xl";
        case 1: return "yl";
        case 2: return "xr";
        case 3: return "yr";
        case 4: return "X";
        case 5: return "Y";
        case 6: return "Z";
        }
    }

    return QVariant();
}

void PointsModel::setLeftPoint( int row, QPointF p ){
    if (row<leftPoints.size()){
        leftPoints[row] = p;
        emit dataChanged( index(row, 0), index(row, 1) );
    }
}

void PointsModel::setRightPoint( int row, QPointF p ){
    if (row<rightPoints.size()){
        rightPoints[row] = p;
        emit dataChanged( index(row, 2), index(row, 3) );
    }
}

void PointsModel::setTriangulatedPoint( int row, cv::Point3f p ){
    if (row<(int)triangulatedPoints.size()){
        triangulatedPoints[row] = p;
        emit dataChanged( index(row, 4), index(row, 6) );
    }
}

void PointsModel::insertRow(){
    beginInsertRows(QModelIndex(), leftPoints.size(), leftPoints.size());
    leftPoints.push_back(QPointF());
    rightPoints.push_back(QPointF());
    triangulatedPoints.push_back(cv::Point3f(0,0,0));
    endInsertRows();
}

void PointsModel::removeRow(int row, const QModelIndex &parent){
    if (parent.isValid())
        return;

    beginRemoveRows(QModelIndex(), row, row);
    leftPoints.removeAt(row);
    rightPoints.removeAt(row);
    triangulatedPoints.erase(triangulatedPoints.begin()+row);
    endRemoveRows();
}

QPointF PointsModel::leftPoint(int row) const{
    if (row<leftPoints.size())
        return leftPoints[row];
    else
        return QPointF();
}
QPointF PointsModel::rightPoint(int row) const{
    if (row<rightPoints.size())
        return rightPoints[row];
    else
        return QPointF();
}
cv::Point3f PointsModel::triangulatedPoint(int row) const{
    if (row<(int)triangulatedPoints.size())
        return triangulatedPoints[row];
    else
        return cv::Point3f(0,0,0);
}



LinesModel::LinesModel(QObject * parent)
    :QAbstractTableModel(parent){
    leftGraphicsGroup = new QGraphicsItemGroup;
    rightGraphicsGroup = new QGraphicsItemGroup;
}
LinesModel::~LinesModel(){

}

int LinesModel::rowCount(const QModelIndex &parent ) const{
    if (parent.isValid())
        return 0;
    else
        return leftLines.size();
}
int LinesModel::columnCount(const QModelIndex &parent ) const{
    if (parent.isValid())
        return 0;
    else
        return 6;
}

QVariant LinesModel::data(const QModelIndex &index, int role) const{
    if (index.parent().isValid())
        return QVariant();

    int row = index.row();

    if (index.column()==5 && (role==Qt::BackgroundColorRole || role == Qt::EditRole))
        return leftGraphicsItems[row]->pen().color();

    if (role==Qt::DisplayRole){
        switch (index.column()){
        case 0: return QString("%1 %2").arg(leftLines[row].p1().x()).arg(leftLines[row].p1().y());
        case 1: return QString("%1 %2").arg(leftLines[row].p2().x()).arg(leftLines[row].p2().y());
        case 2: return QString("%1 %2").arg(rightLines[row].p1().x()).arg(rightLines[row].p1().y());
        case 3: return QString("%1 %2").arg(rightLines[row].p2().x()).arg(rightLines[row].p2().y());
        case 4: return distances[row];
        case 5: return leftGraphicsItems[0]->pen().color();
        default: return QVariant();
        }
    }

    return QVariant();
}

QVariant LinesModel::headerData ( int section, Qt::Orientation orientation, int role ) const{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Vertical)
        return section+1;

    if (orientation == Qt::Horizontal){
        switch (section){
        case 0: return "First Left";
        case 1: return "second Left";
        case 2: return "First Right";
        case 3: return "Second Right";
        case 4: return "Distance";
        case 5: return "Color";
        default: return QVariant();
        }
    }
    return QVariant();
}

bool LinesModel::setData ( const QModelIndex & index, const QVariant & value, int role ){
    if (!index.isValid())
        return false;

    if (index.column()==5 && role==Qt::EditRole && value.canConvert(QVariant::Color)){
        QPen pen = leftGraphicsItems[index.row()]->pen();
        pen.setColor( QColor(value.toString()) );
        leftGraphicsItems[index.row()]->setPen(pen);
        rightGraphicsItems[index.row()]->setPen(pen);
        return true;
    }

    return false;
}

Qt::ItemFlags LinesModel::flags ( const QModelIndex & index ) const{
    if (index.column()==5)
        return Qt::ItemIsEditable | Qt::ItemIsEnabled;
    else
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

void LinesModel::insertRow(){
    beginInsertRows(QModelIndex(), distances.size(), distances.size());
    leftLines.push_back(QLineF());
    rightLines.push_back(QLineF());
    distances.push_back(0);
    endInsertRows();

    leftGraphicsItems.push_back( new QGraphicsLineItem(leftLines.back(), leftGraphicsGroup) );
    rightGraphicsItems.push_back( new QGraphicsLineItem(rightLines.back(), rightGraphicsGroup) );
}

void LinesModel::removeRow(int row, const QModelIndex &parent ){
    if (parent.isValid())
        return;

    beginRemoveRows(QModelIndex(), row, row);
    leftLines.removeAt(row);
    rightLines.removeAt(row);
    distances.removeAt(row);
    endRemoveRows();

    delete leftGraphicsItems.takeAt(row);
    delete rightGraphicsItems.takeAt(row);
}

void LinesModel::setGraphicsImageItems(QGraphicsItem *left, QGraphicsItem *right){
    leftGraphicsGroup->setParentItem(left);
    rightGraphicsGroup->setParentItem(right);
}

void LinesModel::setPoint( int row, PointPosition pos, QPointF p ){
    int column=0;
    switch (pos){
    case FirstLeftPoint:
        leftLines[row].setP1(p); column=0; break;
    case SecondLeftPoint: leftLines[row].setP2(p); column=1; break;
    case FirstRightPoint: rightLines[row].setP1(p); column=2; break;
    case SecondRightPoint: rightLines[row].setP2(p); column=3; break;
    }

    if (column<2)
        leftGraphicsItems[row]->setLine(leftLines[row]);
    else
        rightGraphicsItems[row]->setLine(rightLines[row]);

    emit dataChanged( index(row, column), index(row, column) );
}

void LinesModel::setDistance( int row, float d ){
    distances[row] = d;
    emit dataChanged(index(row, 4), index(row, 4));
}

QLineF LinesModel::leftLine(int row) const{
    return leftLines[row];
}

QLineF LinesModel::rightLine(int row) const{
    return rightLines[row];
}

float LinesModel::distance(int row) const{
    return distances[row];
}

void LinesModel::saveToFile(cv::FileStorage &fs) const{
    fs << "{"
       << "leftLines" << "[";
    for(int i=0; i<leftLines.size(); i++)
        fs << leftLines[i].p1().x() << leftLines[i].p1().y()
           << leftLines[i].p2().x() << leftLines[i].p2().y()
           << leftGraphicsItems[i]->pen().color().name().toStdString();
    fs << "]";

    fs << "rightLines" << "[";
    for(int i=0; i<rightLines.size(); i++)
        fs << rightLines[i].p1().x() << rightLines[i].p1().y()
           << rightLines[i].p2().x() << rightLines[i].p2().y()
           << rightGraphicsItems[i]->pen().color().name().toStdString();
    fs << "]";

    fs << "distances" << distances.toVector().toStdVector()
       << "}";

}

void LinesModel::loadFromFile(const cv::FileNode &n){
    beginResetModel();

    while(rowCount()){
        removeRow(0);
    }

    cv::FileNode node = n["leftLines"];
    for(size_t i=0; i<node.size(); i+=5){
        QLineF line( node[i], node[i+1], node[i+2], node[i+3] );
        QColor color(QString::fromStdString( node[i+4] ));
        leftLines.push_back(line);
        leftGraphicsItems.push_back( new QGraphicsLineItem( line, leftGraphicsGroup ) );
        leftGraphicsItems.back()->setPen( QPen( color ) );
    }

    node = n["rightLines"];
    for(size_t i=0; i<node.size(); i+=5){
        QLineF line( node[i], node[i+1], node[i+2], node[i+3] );
        QColor color(QString::fromStdString( node[i+4] ));
        rightLines.push_back(line);
        rightGraphicsItems.push_back( new QGraphicsLineItem( line, rightGraphicsGroup ) );
        rightGraphicsItems.back()->setPen( QPen( color ) );
    }
    std::vector<float> dist;
    n["distances"] >> dist;
    distances = QList<float>::fromVector(QVector<float>::fromStdVector(dist));

    endResetModel();

}




MeasureWindow::MeasureWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MeasureWindow)
{
    ui->setupUi(this);
    leftPixmap = NULL;
    rightPixmap = NULL;

//    view->setMinimumSize(800, 600);
    scene = new QGraphicsScene;
    ui->view->setScene(scene);

    lines = new LinesModel;
    ui->table->setModel(lines);

	connect(ui->pair, SIGNAL(currentIndexChanged(int)), this, SLOT(loadPair(int)));
    connect(ui->table->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this, SLOT(currentCellChanged(QModelIndex,QModelIndex)));
    connect(ui->view, SIGNAL(clicked(QGraphicsItem*,QPointF)), this, SLOT(pointClicked(QGraphicsItem*,QPointF)));
	connect(ui->view, SIGNAL(dragged(QPointF)), this, SLOT(pointDragged(QPointF)));

    setWindowFlags( windowFlags() & (!Qt::Dialog | Qt::Window) );
}

MeasureWindow::~MeasureWindow()
{
    delete ui;
}


void MeasureWindow::setImageModel(QStandardItemModel *model){
	model_img = model;
	ui->pair->setModel(model);
}

void MeasureWindow::setCalibData(const CalibData &left, const CalibData &right){
    leftCalib = left;
    rightCalib = right;
}

void MeasureWindow::saveToFile(cv::FileStorage &fs) const{
   lines->saveToFile(fs);
}

void MeasureWindow::loadFromFile( const cv::FileNode &n ){
    lines->loadFromFile(n);
}

void MeasureWindow::loadPair(int row){
    lines->setGraphicsImageItems(0, 0);
    if (leftPixmap!= NULL){
        scene->removeItem(leftPixmap);
        delete leftPixmap;
    }
    if (rightPixmap != NULL){
        scene->removeItem(rightPixmap);
        delete rightPixmap;
    }

    if (row<0)
        return;

	QPixmap img(model_img->item(row)->data().toString());

	leftPixmap = new QGraphicsPixmapItem(img.copy(0, 0, img.width(), img.height() / 2));
    scene->addItem( leftPixmap );

	rightPixmap = new QGraphicsPixmapItem(img.copy(0, img.height() / 2, img.width(), img.height() / 2));
    scene->addItem( rightPixmap );

    QModelIndex index = ui->table->currentIndex();
    if (index.isValid() && index.column()>1)
        rightPixmap->setZValue(1);
    else
        leftPixmap->setZValue(1);

    lines->setGraphicsImageItems(leftPixmap, rightPixmap);
}

void MeasureWindow::currentCellChanged(const QModelIndex & current, const QModelIndex & /*previous*/){
    if (!current.isValid())
        return;

    if (current.column()<2){
        leftPixmap->setZValue(1);
        rightPixmap->setZValue(0);
    } else if (current.column()<4){
        leftPixmap->setZValue(0);
        rightPixmap->setZValue(1);
    }

	ui->view->setFocus();
}

void MeasureWindow::on_btAdd_clicked(){
    lines->insertRow();
}

void MeasureWindow::on_btRemove_clicked(){
    lines->removeRow( ui->table->currentIndex().row() );
}

void MeasureWindow::pointClicked(QGraphicsItem * /*item*/, QPointF pos){
    QModelIndex index = ui->table->currentIndex();
    if (!index.isValid() || index.column()>3)
        return;

    int row = index.row();
    lines->setPoint(row, (LinesModel::PointPosition)index.column(), pos);
    if (!lines->leftLine(row).p1().isNull() && !lines->leftLine(row).p2().isNull() &&
        !lines->rightLine(row).p1().isNull() && !lines->rightLine(row).p2().isNull())
        triangulateDistance(index.row());
}

void MeasureWindow::pointDragged( QPointF pos){
    QModelIndex index = ui->table->currentIndex();
    if (!index.isValid() || index.column()>3)
        return;

    int row = index.row();
    lines->setPoint(row, (LinesModel::PointPosition)index.column(), pos);
    if (!lines->leftLine(row).p1().isNull() && !lines->leftLine(row).p2().isNull() &&
        !lines->rightLine(row).p1().isNull() && !lines->rightLine(row).p2().isNull())
        triangulateDistance(index.row());
}

void MeasureWindow::on_btRefine_clicked(){
    //QModelIndex index = ui->table->currentIndex();
    //if (!index.isValid())
    //    return;

    //QLineF leftLine = lines->leftLine(index.row());
    //QLineF rightLine = lines->rightLine(index.row());

    //std::vector<cv::Point2f> leftPoints, rightPoints;
    //leftPoints.push_back( cv::Point2f( leftLine.p1().x(), leftLine.p1().y() ) );
    //leftPoints.push_back( cv::Point2f( leftLine.p2().x(), leftLine.p2().y() ) );
    //rightPoints.push_back( cv::Point2f( rightLine.p1().x(), rightLine.p1().y() ) );
    //rightPoints.push_back( cv::Point2f( rightLine.p2().x(), rightLine.p2().y() ) );

    //int id = ui->pair->currentIndex;
    //refineMatches(leftPoints, rightPoints, leftFiles[id], rightFiles[id]);

    //lines->setPoint(index.row(), LinesModel::FirstLeftPoint, QPointF(leftPoints[0].x, leftPoints[0].y));
    //lines->setPoint(index.row(), LinesModel::SecondLeftPoint, QPointF(leftPoints[1].x, leftPoints[1].y));
    //lines->setPoint(index.row(), LinesModel::FirstRightPoint, QPointF(rightPoints[0].x, rightPoints[0].y));
    //lines->setPoint(index.row(), LinesModel::SecondRightPoint, QPointF(rightPoints[1].x, rightPoints[1].y));

    //triangulateDistance(index.row());

}

void MeasureWindow::triangulatePoint(int row){
    using namespace cv;
    using namespace std;

    if (row<0 || row>=points->rowCount() || !leftCalib.isValid() || !rightCalib.isValid())
        return;

    QPointF l = points->leftPoint(row);
    QPointF r = points->rightPoint(row);
    vector<Point2f> left, right;
    left.push_back( Point2f(l.x(), l.y()) );
    right.push_back( Point2f(r.x(), r.y()) );

    vector<Point3f> pt3d = triangulate(leftCalib, rightCalib, left, right);
    points->setTriangulatedPoint(row, pt3d[0]);

}

void MeasureWindow::triangulateDistance(int row){
    using namespace cv;
    using namespace std;

    if (row<0 || row>=lines->rowCount() || !leftCalib.isValid() || !rightCalib.isValid())
        return;

    vector<Point2f> left, right;
    QPointF p = lines->leftLine(row).p1();
    left.push_back( Point2f(p.x(), p.y()) );
    p = lines->leftLine(row).p2();
    left.push_back( Point2f(p.x(), p.y()) );

    p = lines->rightLine(row).p1();
    right.push_back( Point2f(p.x(), p.y()) );
    p = lines->rightLine(row).p2();
    right.push_back( Point2f(p.x(), p.y()) );

    vector<Point3f> pt3d;
    pt3d = triangulate(leftCalib, rightCalib, left, right);

    float d = norm(pt3d[0]-pt3d[1]);
    lines->setDistance(row, d);
}
