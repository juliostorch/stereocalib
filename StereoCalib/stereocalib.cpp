#include "stereocalib.h"
#include "cvex.h"
#include "measurewindow.h"
#include "verifywindow.h"
#include <qfiledialog.h>
#include <qfileinfo.h>
#include <qstandardpaths.h>
#include <qgraphicsitem.h>
#include <qfuturewatcher.h>
#include <QtConcurrent\qtconcurrentmap.h>
#include <QProgressDialog>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\calib3d\calib3d.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <qmessagebox.h>
#include <qpixmapcache.h>
#include <qdebug.h>

void loadPair(const QString &filename, cv::Mat *left, cv::Mat *right, cv::Mat *full){
	cv::Mat img = cv::imread(filename.toStdString());
	if (img.data == 0)
		return;
	*left = img.rowRange(0, img.rows / 2);
	*right = img.rowRange(img.rows / 2, img.rows);
	*full = img;
}

void saveThumbnail(QString &filename){
	QPixmap img(filename);
	if (img.isNull()){
		filename.clear();
		return;
	}

	QString newname = QString("%1/%2")
		.arg(QStandardPaths::standardLocations(QStandardPaths::TempLocation).first())
		.arg(QFileInfo(filename).fileName());
	if (QFile::exists(newname))
		QFile::remove(newname);

	if (!img/*.scaledToHeight(480)*/.save(newname))
		filename.clear();
}

void saveGridThumbnail(QString &filename, cv::Mat img){

	if (!img.data){
		filename.clear();
		return;
	}

	QString newname = QString("%1/%2")
		.arg(QStandardPaths::standardLocations(QStandardPaths::TempLocation).first())
		.arg(QFileInfo(filename).fileName());
	if (QFile::exists(newname))
		QFile::remove(newname);

	cv::Mat img2;
	float scale = 1;//480.0 / (float)img.rows;
	cv::resize(img, img2, cv::Size(), scale, scale);
	if (!cv::imwrite(newname.toStdString(), img2));
	filename.clear();
}

StereoCalib::StereoCalib(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	KeyFocusFilter *filter = new KeyFocusFilter(this);
	filter->setLabel(ui.l_padrao);
	ui.ptLinha->installEventFilter(filter);
	ui.ptColuna->installEventFilter(filter);
	ui.dHorizontal->installEventFilter(filter);
	ui.dVertical->installEventFilter(filter);
	ui.xOrigem->installEventFilter(filter);
	ui.yOrigem->installEventFilter(filter);

	model_img = new QStandardItemModel(this);
	model_img->setColumnCount(1);
	model_img->setHorizontalHeaderLabels(QStringList() << "Arquivo");
	ui.img_view->setModel(model_img);
	ui.view->setScene(new QGraphicsScene(this));
	ui.view->scene()->addEllipse(QRectF(0, 0, 50, 50), QPen(), Qt::blue);
	ui.actionVisualizar_Imagens->setChecked(true);

	connect(ui.img_view->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)), this, SLOT(on_img_view_currentRowChanged(QModelIndex, QModelIndex)));
}

StereoCalib::~StereoCalib()
{

}

CalibPattern StereoCalib::calibPattern() const {
	return CalibPattern(ui.ptLinha->text().toInt(), ui.ptColuna->text().toInt()
		, cv::Point2f(ui.xOrigem->text().toFloat(), ui.yOrigem->text().toFloat())
		, ui.dHorizontal->text().toFloat(), ui.dVertical->text().toFloat());
}

void StereoCalib::setCalibPattern(const CalibPattern &pattern){
	if (pattern.pointsPerRow > 0)
		ui.ptLinha->setText(QString::number(pattern.pointsPerRow));
	if (pattern.pointsPerColumn > 0)
		ui.ptColuna->setText(QString::number(pattern.pointsPerColumn));
	if (pattern.xDistance > 0)
		ui.dHorizontal->setText(QString::number(pattern.xDistance));
	if (pattern.yDistance > 0)
		ui.dVertical->setText(QString::number(pattern.yDistance));
	if (pattern.origin.x > 0)
		ui.xOrigem->setText(QString::number(pattern.origin.x));
	if (pattern.origin.y > 0)
		ui.yOrigem->setText(QString::number(pattern.origin.y));
}

void StereoCalib::clear(){
	model_img->clear();
	m_leftGrid.clear();
	m_rightGrid.clear();
	m_leftCalib = m_rightCalib = CalibData();
	ui.ptLinha->clear();
	ui.ptColuna->clear();
	ui.dHorizontal->clear();
	ui.dVertical->clear();
	ui.xOrigem->clear();
	ui.yOrigem->clear();
	setWindowTitle("StereoCalib");

}

void StereoCalib::loadImages(const QStringList &filenames){
	QStringList files = filenames;
	QProgressDialog dialog;
	dialog.setLabelText(tr("Carregando arquivos..."));

	// Create a QFutureWatcher and connect signals and slots.
	QFutureWatcher<void> futureWatcher;
	QObject::connect(&futureWatcher, SIGNAL(finished()), &dialog, SLOT(reset()));
	QObject::connect(&dialog, SIGNAL(canceled()), &futureWatcher, SLOT(cancel()));
	QObject::connect(&futureWatcher, SIGNAL(progressRangeChanged(int, int)), &dialog, SLOT(setRange(int, int)));
	QObject::connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &dialog, SLOT(setValue(int)));

	// Start the computation.
	futureWatcher.setFuture(QtConcurrent::map(files, saveThumbnail));

	// Display the dialog and start the event loop.
	dialog.exec();
	futureWatcher.waitForFinished();

	// Query the future to check if was canceled.
	if (futureWatcher.future().isCanceled())
		return;

	for (int i = 0; i < files.size(); i++)
	{
		if (files[i].isEmpty())
			continue;
		QStandardItem *item = new QStandardItem(QFileInfo(files[i]).completeBaseName());
		item->setData(files[i]);
		item->setCheckable(true);
		item->setCheckState(Qt::Checked);
		model_img->appendRow(item);
		m_leftGrid.append(GridPoints());
		m_rightGrid.append(GridPoints());
	}
}

bool StereoCalib::loadThumbnail(const QString &filename){
	QGraphicsPixmapItem *item = new QGraphicsPixmapItem(QString("%1/%2")
		.arg(QStandardPaths::standardLocations(QStandardPaths::TempLocation).first())
		.arg(QFileInfo(filename).completeBaseName()));
	if (item->pixmap().isNull()){
		delete item;
		return false;
	}
		
	ui.view->scene()->clear();
	ui.view->scene()->addItem(item);
	return true;
	
}

void StereoCalib::findGridInPair(int id){
	cv::Mat left, right, full;
	loadPair(model_img->item(id)->data().toString(), &left, &right, &full);
	cv::Size size = calibPattern().patternSize();

	m_leftGrid[id] = findGrid(left, size);
	if (m_leftGrid[id].size()){
		m_rightGrid[id] = findGrid(right, size);
		if (m_rightGrid[id].size()){
			cv::drawChessboardCorners(left, size, m_leftGrid[id], true);
			cv::drawChessboardCorners(right, size, m_rightGrid[id], true);
			saveGridThumbnail(model_img->item(id)->data().toString(), full);
		}
	}
	
}

void StereoCalib::drawGridInPairImage(int id){
	cv::Mat left, right, full;
	loadPair(model_img->item(id)->data().toString(), &left, &right, &full);
	cv::Size size = calibPattern().patternSize();

	if (m_leftGrid[id].size()){
		if (m_rightGrid[id].size()){
			cv::drawChessboardCorners(left, size, m_leftGrid[id], true);
			cv::drawChessboardCorners(right, size, m_rightGrid[id], true);
			saveGridThumbnail(model_img->item(id)->data().toString(), full);
		}
	}

}

bool StereoCalib::save(const QString &filename){
	cv::FileStorage fs(filename.toStdString(), cv::FileStorage::WRITE);
	if (!fs.isOpened())
		return false;

	fs << "pattern" << calibPattern()
		<< "leftCalib" << m_leftCalib
		<< "rightCalib" << m_rightCalib;

	fs << "files" << "[";
	for (int i = 0; i < model_img->rowCount(); i++)
		fs << model_img->item(i)->data().toString().toStdString()
			<< (int)model_img->item(i)->checkState();
	fs << "]";


	fs << "leftGrids" << "[";
	for (int i = 0; i < m_leftGrid.size(); i++){
		fs << "[";
		foreach(cv::Point2f p, m_leftGrid[i])
			fs << p.x << p.y;
		fs << "]";
	}
	fs << "]";

	fs << "rightGrids" << "[";
	for (int i = 0; i < m_rightGrid.size(); i++){
		fs << "[";
		foreach(cv::Point2f p, m_rightGrid[i])
			fs << p.x << p.y;
		fs << "]";
	}
	fs << "]";


	fs.release();
	return true;
}

bool StereoCalib::load(const QString &filename){
	cv::FileStorage fs(filename.toStdString(), cv::FileStorage::READ);
	if (!fs.isOpened())
		return false;
	clear();

	CalibPattern pattern;
	fs["pattern"] >> pattern;
	fs["leftCalib"] >> m_leftCalib;
	fs["rightCalib"] >> m_rightCalib;

	cv::FileNode n = fs["files"];
	QStringList files;
	QList<int> checkStates;
	for (int i = 0; i < n.size(); i += 2){
		files << QString::fromStdString(n[i]);
		checkStates << n[i + 1];
	}


	setCalibPattern(pattern);
	loadImages(files);

	n = fs["leftGrids"];
	for (int i = 0; i < n.size(); i++){
		cv::FileNode ni = n[i];
		std::vector<cv::Point2f> grid;
		for (int j = 0; j < ni.size(); j += 2)
			grid.push_back(cv::Point2f(ni[j], ni[j + 1]));
		m_leftGrid[i] = grid;
	}

	n = fs["rightGrids"];
	for (int i = 0; i < n.size(); i++){
		cv::FileNode ni = n[i];
		std::vector<cv::Point2f> grid;
		for (int j = 0; j < ni.size(); j += 2)
			grid.push_back(cv::Point2f(ni[j], ni[j + 1]));
		m_rightGrid[i] = grid;
	}

	fs.release();

	//Desenha grid nas imagens
	QList<int> rows;
	for (int i = 0; i < model_img->rowCount(); i++)
		rows << i;

	QFutureWatcher<void> futureWatcher;
	QProgressDialog dialog;
	dialog.setLabelText(tr("Buscando padrões em imagens..."));
	connect(&futureWatcher, SIGNAL(progressRangeChanged(int, int)), &dialog, SLOT(setRange(int, int)));
	connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &dialog, SLOT(setValue(int)));
	connect(&futureWatcher, SIGNAL(finished()), &dialog, SLOT(reset()));
	connect(&futureWatcher, SIGNAL(canceled()), &dialog, SLOT(cancel()));

	futureWatcher.setFuture(QtConcurrent::map(rows, [this](int id) { drawGridInPairImage(id); }));
	dialog.exec();
	futureWatcher.waitForFinished();

	//marca de verde as imagens com padrões encontrados
	foreach(int row, rows){
		if (m_leftGrid[row].size() && m_rightGrid[row].size())
			model_img->item(row)->setBackground(QColor::fromRgb(200,255,200));
		else{
			model_img->item(row)->setBackground(QBrush());
		}
		model_img->item(row)->setCheckState((Qt::CheckState)checkStates[row]);
	}
	QPixmapCache::clear();
	on_img_view_currentRowChanged(ui.img_view->currentIndex(), QModelIndex());

	if (m_leftCalib.isValid() && m_rightCalib.isValid())
		setWindowTitle("StereoCalib - Calibrado");
	else
		setWindowTitle("StereoCalib ");


	return true;
}

void StereoCalib::on_bt_adiciona_clicked(){
	QSettings stt;
	QString dir = stt.value("imgDir").toString();
	QStringList files = QFileDialog::getOpenFileNames(this, tr("Adicionar Imagens"), dir, tr("Imagens (*.png *.xpm *.jpg *.bmp *.tif)"));
	if (files.isEmpty())
		return;
	dir = QFileInfo(files.first()).absoluteDir().path();
	stt.setValue("imgDir", dir);

	loadImages(files);
}

void StereoCalib::on_bt_remove_clicked(){
	QModelIndexList list = ui.img_view->selectionModel()->selectedRows();
	QList<int> rows;
	foreach(QModelIndex id, list)
		rows << id.row();
	qSort(rows);
	for (int i = rows.size() - 1; i >= 0; i--){
		model_img->removeRow(rows[i]);
		m_leftGrid.removeAt(i);
		m_rightGrid.removeAt(i);
	}
}

void StereoCalib::on_bt_habilita_clicked(){
	QModelIndexList list = ui.img_view->selectionModel()->selectedIndexes();
	foreach(QModelIndex id, list)
		model_img->item(id.row())->setCheckState(Qt::Checked);
}

void StereoCalib::on_bt_desabilita_clicked(){
	QModelIndexList list = ui.img_view->selectionModel()->selectedIndexes();
	foreach(QModelIndex id, list)
		model_img->item(id.row())->setCheckState(Qt::Unchecked);
}

void StereoCalib::on_bt_limpa_clicked(){
	m_leftGrid.clear();
	m_rightGrid.clear();
	model_img->clear();
	QPixmapCache::clear();
}

void StereoCalib::on_actionVisualizar_Imagens_toggled(bool checked){
	ui.dw_visualizacao->setVisible(checked);
}

void StereoCalib::on_img_view_currentRowChanged(const QModelIndex & current, const QModelIndex & previous){
	if (current.isValid())
		loadThumbnail( model_img->item(current.row())->text() );
}


void StereoCalib::on_actionBuscar_Padroes_triggered()
{
	if (!calibPattern().isValid()){
		QMessageBox::critical(this, tr("Buscar Padrões"), tr("As configurações do padrão não foram preenchidas corretamente!"));
		return;
	}

	QList<int> rows;
	for (int i = 0; i < model_img->rowCount(); i++)
		if (model_img->item(i)->checkState() == Qt::Checked)
			rows << i;

	QFutureWatcher<void> futureWatcher;
	QProgressDialog dialog;
	dialog.setLabelText(tr("Buscando padrões em imagens..."));
	connect(&futureWatcher, SIGNAL(progressRangeChanged(int, int)), &dialog, SLOT(setRange(int, int)));
	connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &dialog, SLOT(setValue(int)));
	connect(&futureWatcher, SIGNAL(finished()), &dialog, SLOT(reset()));
	connect(&futureWatcher, SIGNAL(canceled()), &dialog, SLOT(cancel()));

	futureWatcher.setFuture( QtConcurrent::map(rows, [this](int id) { findGridInPair(id); }) );
	dialog.exec();
	futureWatcher.waitForFinished();

	foreach(int row, rows){
		if (m_leftGrid[row].size() && m_rightGrid[row].size())
			model_img->item(row)->setBackground(QColor::fromRgb(200, 255, 200));
		else{
			model_img->item(row)->setBackground(QBrush());
			model_img->item(row)->setCheckState(Qt::Unchecked);
		}
	}
	QPixmapCache::clear();
	on_img_view_currentRowChanged(ui.img_view->currentIndex(), QModelIndex());
}

void StereoCalib::on_actionCalibrar_triggered(){
	std::vector<cv::Point3f> grid = calibPattern().getPatternCenters();
	std::vector<std::vector<cv::Point2f> > rightCenters, leftCenters;
	std::vector<std::vector<cv::Point3f> > patternCenters;
	cv::Size imageSize;

	for (int i = 0; i < model_img->rowCount(); i++){
		if (model_img->item(i)->checkState() == Qt::Checked){
			leftCenters.push_back(m_leftGrid[i]);
			rightCenters.push_back(m_rightGrid[i]);
			patternCenters.push_back(grid);
			if (imageSize.width == 0){
				QPixmap p(model_img->item(i)->data().toString());
				imageSize.width = p.width();
				imageSize.height = p.height()/2;
			}

		}
	}

	m_leftCalib = calibrate(imageSize, leftCenters, patternCenters);//encontra estimativa inicial para parâmetros intrínsecos
	m_rightCalib = calibrate(imageSize, rightCenters, patternCenters);//encontra estimativa inicial para parâmetros intrínsecos;
	calibrate(imageSize, m_leftCalib, m_rightCalib, leftCenters, rightCenters, patternCenters);//calibra par estéreo
	QMessageBox::information(this, tr("Calibração concluída."), QString(tr("Erro de calibração: %1")).arg(m_leftCalib.reprojectionError));
	if (m_leftCalib.isValid() && m_rightCalib.isValid())
		setWindowTitle("StereoCalib - Calibrado");
	else
		setWindowTitle("StereoCalib ");
}

void StereoCalib::on_actionSalvar_triggered(){
	if (m_filename.isEmpty())
		on_actionSalvar_Como_triggered();
	else
		save(m_filename);
}

void StereoCalib::on_actionSalvar_Como_triggered(){
	QSettings stt;
	QString dir = stt.value("fileDir").toString();
	QString filename = QFileDialog::getSaveFileName(this, "Salvar", dir, "(*.yml)");
	if (filename.isEmpty())
		return;
	dir = QFileInfo(filename).absoluteDir().path();
	stt.setValue("fileDir", dir);

	if (save(filename))
		m_filename = filename;
}

void StereoCalib::on_actionAbrir_triggered(){
	QSettings stt;
	QString dir = stt.value("fileDir").toString();
	QString filename = QFileDialog::getOpenFileName(this, "Abrir", dir, "(*.yml)");
	if (filename.isEmpty())
		return;
	dir = QFileInfo(filename).absoluteDir().path();
	stt.setValue("fileDir", dir);

	if (load(filename))
		m_filename = filename;
}

void StereoCalib::on_actionNovo_triggered(){
	clear();
	m_filename.clear();
}

void StereoCalib::on_actionExportar_para_SVE_triggered(){
	QSettings stt;
	QString dir = stt.value("fileDir").toString();
	QString filename = QFileDialog::getSaveFileName(this, "Exportar para SVE", dir, "(*.par)");
	if (filename.isEmpty())
		return;
	dir = QFileInfo(filename).absoluteDir().path();
	stt.setValue("fileDir", dir);


	cv::Mat_d left = m_leftCalib.toSVEVector();
	cv::Mat_d right = m_rightCalib.toSVEVector();

	QVector<double> l = QVector<double>::fromStdVector(left);
	QVector<double> r = QVector<double>::fromStdVector(right);

	QFile file(filename);
	if (!file.open(QFile::WriteOnly | QFile::Text)){
		QMessageBox::critical(this, "Exportar para SVE", "Não foi possível abrir o arquivo para gravação!");
		return;
	}

	QTextStream out(&file);
	for (int i = 0; i < l.size(); i++)
		out << l[i] << " " << r[i] << endl;
}

void StereoCalib::on_actionMedir_triggered(){
	MeasureWindow measure(this);
	measure.setImageModel(model_img);
	measure.setCalibData(m_leftCalib, m_rightCalib);
	measure.exec();
}

void StereoCalib::on_actionAferir_triggered(){
	VerifyWindow verify(this);
	verify.setUp(model_img, calibPattern(), m_leftCalib, m_rightCalib, &m_leftGrid, &m_rightGrid);
	verify.exec();
}